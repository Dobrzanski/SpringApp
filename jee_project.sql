-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Czas generowania: 13 Cze 2016, 12:48
-- Wersja serwera: 5.6.25
-- Wersja PHP: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `jee_project`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `building`
--

CREATE TABLE IF NOT EXISTS `building` (
  `buildingId` int(11) NOT NULL,
  `city` varchar(255) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `building`
--

INSERT INTO `building` (`buildingId`, `city`, `number`, `street`) VALUES
(1, 'Pabianice', 34, 'Pasterska'),
(2, 'Katowice', 2, 'Zamkowa'),
(3, 'Pabianice', 12, 'Pasterska');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `chargeelectricity`
--

CREATE TABLE IF NOT EXISTS `chargeelectricity` (
  `chargeId` int(11) NOT NULL,
  `amountToPay` double DEFAULT NULL,
  `finalState` double DEFAULT NULL,
  `initialState` double DEFAULT NULL,
  `name` varchar(255) COLLATE cp1250_polish_ci DEFAULT NULL,
  `paymentDate` date DEFAULT NULL,
  `pricePerUnit` double DEFAULT NULL,
  `status` bit(1) DEFAULT NULL,
  `captchaCode` varchar(255) COLLATE cp1250_polish_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=cp1250 COLLATE=cp1250_polish_ci;

--
-- Zrzut danych tabeli `chargeelectricity`
--

INSERT INTO `chargeelectricity` (`chargeId`, `amountToPay`, `finalState`, `initialState`, `name`, `paymentDate`, `pricePerUnit`, `status`, `captchaCode`) VALUES
(1, 776.06, 15, 5.6, 'Op?ata za pr?d | CZERWIEC', '2016-07-08', 82.56, b'1', 'NY8SH'),
(5, 2.83, 8, 5.6, 'Op?ata za pr?d | SIERPIE?', '2016-09-10', 1.18, b'1', '9RD8PT');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `chargegarbage`
--

CREATE TABLE IF NOT EXISTS `chargegarbage` (
  `chargeId` int(11) NOT NULL,
  `amountToPay` double DEFAULT NULL,
  `name` varchar(255) COLLATE cp1250_polish_ci DEFAULT NULL,
  `numberOfPeople` int(11) DEFAULT NULL,
  `paymentDate` date DEFAULT NULL,
  `pricePerPerson` double DEFAULT NULL,
  `status` bit(1) DEFAULT NULL,
  `captchaCode` varchar(255) COLLATE cp1250_polish_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=cp1250 COLLATE=cp1250_polish_ci;

--
-- Zrzut danych tabeli `chargegarbage`
--

INSERT INTO `chargegarbage` (`chargeId`, `amountToPay`, `name`, `numberOfPeople`, `paymentDate`, `pricePerPerson`, `status`, `captchaCode`) VALUES
(3, 51, 'Op?ata za ?mieci | CZERWIEC', 6, '2016-07-23', 8.5, b'1', 'PEEJ'),
(4, 120, 'Op?ata za ?mieci | LIPIEC', 6, '2016-08-18', 20, b'0', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `chargegas`
--

CREATE TABLE IF NOT EXISTS `chargegas` (
  `chargeId` int(11) NOT NULL,
  `amountToPay` double DEFAULT NULL,
  `finalState` double DEFAULT NULL,
  `initialState` double DEFAULT NULL,
  `name` varchar(255) COLLATE cp1250_polish_ci DEFAULT NULL,
  `paymentDate` date DEFAULT NULL,
  `pricePerUnit` double DEFAULT NULL,
  `status` bit(1) DEFAULT NULL,
  `captchaCode` varchar(255) COLLATE cp1250_polish_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=cp1250 COLLATE=cp1250_polish_ci;

--
-- Zrzut danych tabeli `chargegas`
--

INSERT INTO `chargegas` (`chargeId`, `amountToPay`, `finalState`, `initialState`, `name`, `paymentDate`, `pricePerUnit`, `status`, `captchaCode`) VALUES
(1, 222.6, 32, 18, 'Op?ata za gaz | CZERWIEC', '2016-07-22', 15.9, b'1', 'MY4K'),
(2, 0, 0, 18, 'Op?ata za gaz | LIPIEC', '2016-08-25', 13, b'0', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `chargeheating`
--

CREATE TABLE IF NOT EXISTS `chargeheating` (
  `chargeId` int(11) NOT NULL,
  `amountToPay` double DEFAULT NULL,
  `finalState` double DEFAULT NULL,
  `initialState` double DEFAULT NULL,
  `name` varchar(255) COLLATE cp1250_polish_ci DEFAULT NULL,
  `paymentDate` date DEFAULT NULL,
  `pricePerUnit` double DEFAULT NULL,
  `status` bit(1) DEFAULT NULL,
  `captchaCode` varchar(255) COLLATE cp1250_polish_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=cp1250 COLLATE=cp1250_polish_ci;

--
-- Zrzut danych tabeli `chargeheating`
--

INSERT INTO `chargeheating` (`chargeId`, `amountToPay`, `finalState`, `initialState`, `name`, `paymentDate`, `pricePerUnit`, `status`, `captchaCode`) VALUES
(1, 113.5, 58, 12.6, 'Op?ata za ogrzewanie | 2015', '2016-06-18', 2.5, b'1', '46V4VE'),
(2, 0, 0, 12.6, 'Op?ata za ogrzewanie | 2016', '2017-06-16', 1.85, b'0', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `chargeother`
--

CREATE TABLE IF NOT EXISTS `chargeother` (
  `chargeId` int(11) NOT NULL,
  `amountToPay` double DEFAULT NULL,
  `name` varchar(255) COLLATE cp1250_polish_ci DEFAULT NULL,
  `paymentDate` date DEFAULT NULL,
  `status` bit(1) DEFAULT NULL,
  `captchaCode` varchar(255) COLLATE cp1250_polish_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=cp1250 COLLATE=cp1250_polish_ci;

--
-- Zrzut danych tabeli `chargeother`
--

INSERT INTO `chargeother` (`chargeId`, `amountToPay`, `name`, `paymentDate`, `status`, `captchaCode`) VALUES
(1, 120, 'Op?ata inna | DOMOFON', '2016-09-09', b'1', 'WD96K3'),
(2, 65, 'Op?ata inna | TRAWNIK', '2016-06-24', b'0', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `chargesevage`
--

CREATE TABLE IF NOT EXISTS `chargesevage` (
  `chargeId` int(11) NOT NULL,
  `amountToPay` double DEFAULT NULL,
  `finalState` double DEFAULT NULL,
  `initialState` double DEFAULT NULL,
  `name` varchar(255) COLLATE cp1250_polish_ci DEFAULT NULL,
  `paymentDate` date DEFAULT NULL,
  `pricePerUnit` double DEFAULT NULL,
  `status` bit(1) DEFAULT NULL,
  `captchaCode` varchar(255) COLLATE cp1250_polish_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=cp1250 COLLATE=cp1250_polish_ci;

--
-- Zrzut danych tabeli `chargesevage`
--

INSERT INTO `chargesevage` (`chargeId`, `amountToPay`, `finalState`, `initialState`, `name`, `paymentDate`, `pricePerUnit`, `status`, `captchaCode`) VALUES
(1, 26, 14, 9, 'Op?ata za ?cieki | CZERWIEC', '2016-07-21', 5.2, b'1', 'S845J'),
(2, 0, 0, 9, 'Op?ata za ?cieki | LIPIEC', '2016-08-20', 3.89, b'0', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `chargewater`
--

CREATE TABLE IF NOT EXISTS `chargewater` (
  `chargeId` int(11) NOT NULL,
  `amountToPay` double DEFAULT NULL,
  `finalStateCold` double DEFAULT NULL,
  `finalStateHot` double DEFAULT NULL,
  `initialStateCold` double DEFAULT NULL,
  `initialStateHot` double DEFAULT NULL,
  `name` varchar(255) COLLATE cp1250_polish_ci DEFAULT NULL,
  `paymentDate` date DEFAULT NULL,
  `pricePerUnitCold` double DEFAULT NULL,
  `pricePerUnitHot` double DEFAULT NULL,
  `status` bit(1) DEFAULT NULL,
  `captchaCode` varchar(255) COLLATE cp1250_polish_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=cp1250 COLLATE=cp1250_polish_ci;

--
-- Zrzut danych tabeli `chargewater`
--

INSERT INTO `chargewater` (`chargeId`, `amountToPay`, `finalStateCold`, `finalStateHot`, `initialStateCold`, `initialStateHot`, `name`, `paymentDate`, `pricePerUnitCold`, `pricePerUnitHot`, `status`, `captchaCode`) VALUES
(1, 50.11, 14.99, 15, 3.2, 14.9, 'Op?ata za wod? | CZERWIEC', '2016-07-22', 4.2, 5.9, b'1', 'SS5N'),
(2, 0, 0, 0, 3.2, 14.9, 'Op?ata za wod? | LIPIEC', '2016-08-12', 3.4, 8.2, b'0', NULL);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `userId` int(11) NOT NULL,
  `buildingNumber` int(11) DEFAULT NULL,
  `city` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `firstname` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `flatNumber` int(11) DEFAULT NULL,
  `mail` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `password` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `peopleNumber` int(11) DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `street` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `surname` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `stateColdWater` double DEFAULT NULL,
  `stateElectricity` double DEFAULT NULL,
  `stateGas` double DEFAULT NULL,
  `stateHeating` double DEFAULT NULL,
  `stateHotWater` double DEFAULT NULL,
  `stateSevage` double DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `user`
--

INSERT INTO `user` (`userId`, `buildingNumber`, `city`, `enabled`, `firstname`, `flatNumber`, `mail`, `password`, `peopleNumber`, `phone`, `street`, `surname`, `stateColdWater`, `stateElectricity`, `stateGas`, `stateHeating`, `stateHotWater`, `stateSevage`) VALUES
(1, 34, 'Pabianice', b'1', 'Dawid', 7, 'dawid8493@gmail.com', '$2a$10$xkLfmPvoSt1EYutLwae1denA9OpWC0ToV49T0wcIdTOrPeJkcsPKW', 2, '530-445-864', 'Pasterska', 'Dobrza?ski', 20.5, 20.4, 20.5, 30, 20.7, 40.5),
(8, 2, 'Katowice', b'1', 'Karolina', 3, 'tester2@test.pl', '$2a$10$xkLfmPvoSt1EYutLwae1denA9OpWC0ToV49T0wcIdTOrPeJkcsPKW', 6, '530-852-854', 'Pasterska', 'Szczepaniak', 3.2, 5.6, 18, 12.6, 14.9, 9);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `userrole`
--

CREATE TABLE IF NOT EXISTS `userrole` (
  `id` int(11) NOT NULL,
  `role` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `userrole`
--

INSERT INTO `userrole` (`id`, `role`) VALUES
(1, 'ROLE_ADMIN'),
(2, 'ROLE_USER');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user_chargeelectricity`
--

CREATE TABLE IF NOT EXISTS `user_chargeelectricity` (
  `User_userId` int(11) NOT NULL,
  `chargeElectricity_chargeId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1250 COLLATE=cp1250_polish_ci;

--
-- Zrzut danych tabeli `user_chargeelectricity`
--

INSERT INTO `user_chargeelectricity` (`User_userId`, `chargeElectricity_chargeId`) VALUES
(8, 1),
(8, 5);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user_chargegarbage`
--

CREATE TABLE IF NOT EXISTS `user_chargegarbage` (
  `User_userId` int(11) NOT NULL,
  `chargeGarbage_chargeId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1250 COLLATE=cp1250_polish_ci;

--
-- Zrzut danych tabeli `user_chargegarbage`
--

INSERT INTO `user_chargegarbage` (`User_userId`, `chargeGarbage_chargeId`) VALUES
(8, 3),
(8, 4);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user_chargegas`
--

CREATE TABLE IF NOT EXISTS `user_chargegas` (
  `User_userId` int(11) NOT NULL,
  `chargeGas_chargeId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1250 COLLATE=cp1250_polish_ci;

--
-- Zrzut danych tabeli `user_chargegas`
--

INSERT INTO `user_chargegas` (`User_userId`, `chargeGas_chargeId`) VALUES
(8, 1),
(8, 2);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user_chargeheating`
--

CREATE TABLE IF NOT EXISTS `user_chargeheating` (
  `User_userId` int(11) NOT NULL,
  `chargeHeating_chargeId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1250 COLLATE=cp1250_polish_ci;

--
-- Zrzut danych tabeli `user_chargeheating`
--

INSERT INTO `user_chargeheating` (`User_userId`, `chargeHeating_chargeId`) VALUES
(8, 1),
(8, 2);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user_chargeother`
--

CREATE TABLE IF NOT EXISTS `user_chargeother` (
  `User_userId` int(11) NOT NULL,
  `chargeOther_chargeId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1250 COLLATE=cp1250_polish_ci;

--
-- Zrzut danych tabeli `user_chargeother`
--

INSERT INTO `user_chargeother` (`User_userId`, `chargeOther_chargeId`) VALUES
(8, 1),
(8, 2);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user_chargesevage`
--

CREATE TABLE IF NOT EXISTS `user_chargesevage` (
  `User_userId` int(11) NOT NULL,
  `chargeSevage_chargeId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1250 COLLATE=cp1250_polish_ci;

--
-- Zrzut danych tabeli `user_chargesevage`
--

INSERT INTO `user_chargesevage` (`User_userId`, `chargeSevage_chargeId`) VALUES
(8, 1),
(8, 2);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user_chargewater`
--

CREATE TABLE IF NOT EXISTS `user_chargewater` (
  `User_userId` int(11) NOT NULL,
  `chargeWater_chargeId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1250 COLLATE=cp1250_polish_ci;

--
-- Zrzut danych tabeli `user_chargewater`
--

INSERT INTO `user_chargewater` (`User_userId`, `chargeWater_chargeId`) VALUES
(8, 1),
(8, 2);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user_userrole`
--

CREATE TABLE IF NOT EXISTS `user_userrole` (
  `User_userId` int(11) NOT NULL,
  `userRole_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `user_userrole`
--

INSERT INTO `user_userrole` (`User_userId`, `userRole_id`) VALUES
(1, 1),
(8, 2);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `building`
--
ALTER TABLE `building`
  ADD PRIMARY KEY (`buildingId`);

--
-- Indexes for table `chargeelectricity`
--
ALTER TABLE `chargeelectricity`
  ADD PRIMARY KEY (`chargeId`);

--
-- Indexes for table `chargegarbage`
--
ALTER TABLE `chargegarbage`
  ADD PRIMARY KEY (`chargeId`);

--
-- Indexes for table `chargegas`
--
ALTER TABLE `chargegas`
  ADD PRIMARY KEY (`chargeId`);

--
-- Indexes for table `chargeheating`
--
ALTER TABLE `chargeheating`
  ADD PRIMARY KEY (`chargeId`);

--
-- Indexes for table `chargeother`
--
ALTER TABLE `chargeother`
  ADD PRIMARY KEY (`chargeId`);

--
-- Indexes for table `chargesevage`
--
ALTER TABLE `chargesevage`
  ADD PRIMARY KEY (`chargeId`);

--
-- Indexes for table `chargewater`
--
ALTER TABLE `chargewater`
  ADD PRIMARY KEY (`chargeId`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userId`);

--
-- Indexes for table `userrole`
--
ALTER TABLE `userrole`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_chargeelectricity`
--
ALTER TABLE `user_chargeelectricity`
  ADD PRIMARY KEY (`User_userId`,`chargeElectricity_chargeId`),
  ADD KEY `FK_cs1502a0lngsn7vsh0vaf35d0` (`chargeElectricity_chargeId`);

--
-- Indexes for table `user_chargegarbage`
--
ALTER TABLE `user_chargegarbage`
  ADD PRIMARY KEY (`User_userId`,`chargeGarbage_chargeId`),
  ADD KEY `FK_rrjo2a2bbkjalebmbnh6tnn2k` (`chargeGarbage_chargeId`);

--
-- Indexes for table `user_chargegas`
--
ALTER TABLE `user_chargegas`
  ADD PRIMARY KEY (`User_userId`,`chargeGas_chargeId`),
  ADD KEY `FK_73p62mc2h24qbaobkcv2sqmc6` (`chargeGas_chargeId`);

--
-- Indexes for table `user_chargeheating`
--
ALTER TABLE `user_chargeheating`
  ADD PRIMARY KEY (`User_userId`,`chargeHeating_chargeId`),
  ADD KEY `FK_ahf60oo1d0fse05u0qfaih0ty` (`chargeHeating_chargeId`);

--
-- Indexes for table `user_chargeother`
--
ALTER TABLE `user_chargeother`
  ADD PRIMARY KEY (`User_userId`,`chargeOther_chargeId`),
  ADD KEY `FK_7tquax8ttyxc2w1mh25cudsys` (`chargeOther_chargeId`);

--
-- Indexes for table `user_chargesevage`
--
ALTER TABLE `user_chargesevage`
  ADD PRIMARY KEY (`User_userId`,`chargeSevage_chargeId`),
  ADD KEY `FK_x6jvy6imc31toq6tn3yu89ry` (`chargeSevage_chargeId`);

--
-- Indexes for table `user_chargewater`
--
ALTER TABLE `user_chargewater`
  ADD PRIMARY KEY (`User_userId`,`chargeWater_chargeId`),
  ADD KEY `FK_40afpi3rdqtqkrjyo0upct8ce` (`chargeWater_chargeId`);

--
-- Indexes for table `user_userrole`
--
ALTER TABLE `user_userrole`
  ADD PRIMARY KEY (`User_userId`,`userRole_id`),
  ADD KEY `FK_lr2ksams5rjgculd1ai6c5bo9` (`userRole_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `building`
--
ALTER TABLE `building`
  MODIFY `buildingId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT dla tabeli `chargeelectricity`
--
ALTER TABLE `chargeelectricity`
  MODIFY `chargeId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT dla tabeli `chargegarbage`
--
ALTER TABLE `chargegarbage`
  MODIFY `chargeId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT dla tabeli `chargegas`
--
ALTER TABLE `chargegas`
  MODIFY `chargeId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT dla tabeli `chargeheating`
--
ALTER TABLE `chargeheating`
  MODIFY `chargeId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT dla tabeli `chargeother`
--
ALTER TABLE `chargeother`
  MODIFY `chargeId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT dla tabeli `chargesevage`
--
ALTER TABLE `chargesevage`
  MODIFY `chargeId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT dla tabeli `chargewater`
--
ALTER TABLE `chargewater`
  MODIFY `chargeId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT dla tabeli `user`
--
ALTER TABLE `user`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT dla tabeli `userrole`
--
ALTER TABLE `userrole`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `user_chargeelectricity`
--
ALTER TABLE `user_chargeelectricity`
  ADD CONSTRAINT `FK_cs1502a0lngsn7vsh0vaf35d0` FOREIGN KEY (`chargeElectricity_chargeId`) REFERENCES `chargeelectricity` (`chargeId`),
  ADD CONSTRAINT `FK_p8aa83jxx5envkw7cjifp0i0r` FOREIGN KEY (`User_userId`) REFERENCES `user` (`userId`);

--
-- Ograniczenia dla tabeli `user_chargegarbage`
--
ALTER TABLE `user_chargegarbage`
  ADD CONSTRAINT `FK_7ls0vnl83qa3vn4cc05bg6gbu` FOREIGN KEY (`User_userId`) REFERENCES `user` (`userId`),
  ADD CONSTRAINT `FK_rrjo2a2bbkjalebmbnh6tnn2k` FOREIGN KEY (`chargeGarbage_chargeId`) REFERENCES `chargegarbage` (`chargeId`);

--
-- Ograniczenia dla tabeli `user_chargegas`
--
ALTER TABLE `user_chargegas`
  ADD CONSTRAINT `FK_73p62mc2h24qbaobkcv2sqmc6` FOREIGN KEY (`chargeGas_chargeId`) REFERENCES `chargegas` (`chargeId`),
  ADD CONSTRAINT `FK_nrir9x7s6ev09xogd09s4ge3a` FOREIGN KEY (`User_userId`) REFERENCES `user` (`userId`);

--
-- Ograniczenia dla tabeli `user_chargeheating`
--
ALTER TABLE `user_chargeheating`
  ADD CONSTRAINT `FK_1uufv4lmfoxj8wh5oun86w9y9` FOREIGN KEY (`User_userId`) REFERENCES `user` (`userId`),
  ADD CONSTRAINT `FK_ahf60oo1d0fse05u0qfaih0ty` FOREIGN KEY (`chargeHeating_chargeId`) REFERENCES `chargeheating` (`chargeId`);

--
-- Ograniczenia dla tabeli `user_chargeother`
--
ALTER TABLE `user_chargeother`
  ADD CONSTRAINT `FK_7tquax8ttyxc2w1mh25cudsys` FOREIGN KEY (`chargeOther_chargeId`) REFERENCES `chargeother` (`chargeId`),
  ADD CONSTRAINT `FK_hh8jmgsxstm212lb7kx2rbf69` FOREIGN KEY (`User_userId`) REFERENCES `user` (`userId`);

--
-- Ograniczenia dla tabeli `user_chargesevage`
--
ALTER TABLE `user_chargesevage`
  ADD CONSTRAINT `FK_ptraxxtisn1h5p2qr3wvb62p3` FOREIGN KEY (`User_userId`) REFERENCES `user` (`userId`),
  ADD CONSTRAINT `FK_x6jvy6imc31toq6tn3yu89ry` FOREIGN KEY (`chargeSevage_chargeId`) REFERENCES `chargesevage` (`chargeId`);

--
-- Ograniczenia dla tabeli `user_chargewater`
--
ALTER TABLE `user_chargewater`
  ADD CONSTRAINT `FK_40afpi3rdqtqkrjyo0upct8ce` FOREIGN KEY (`chargeWater_chargeId`) REFERENCES `chargewater` (`chargeId`),
  ADD CONSTRAINT `FK_o8cn8hvvfydnh4jd9pfo1rceq` FOREIGN KEY (`User_userId`) REFERENCES `user` (`userId`);

--
-- Ograniczenia dla tabeli `user_userrole`
--
ALTER TABLE `user_userrole`
  ADD CONSTRAINT `FK_kf5wh5kueiaw99k48m360cko1` FOREIGN KEY (`User_userId`) REFERENCES `user` (`userId`),
  ADD CONSTRAINT `FK_lr2ksams5rjgculd1ai6c5bo9` FOREIGN KEY (`userRole_id`) REFERENCES `userrole` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
