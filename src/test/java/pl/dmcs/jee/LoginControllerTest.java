package pl.dmcs.jee;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.Resource;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(
        loader=WebContextLoader.class,
        value={
                "classpath:/META-INF/root-context.xml",
                "classpath:/META-INF/servlet-context.xml",
                "classpath:/META-INF/spring-security.xml"
        })
public class LoginControllerTest {

    private static String SEC_CONTEXT_ATTR = HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY;

    @Autowired
    private FilterChainProxy springSecurityFilterChain;

    @Resource
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac)
                .addFilter(springSecurityFilterChain)
                .build();
    }

    @Test
    public void requiresAuthentication() throws Exception {
        mockMvc.perform(get("/user"))
                .andExpect(redirectedUrl("http://localhost/login.html"));
    }

    @Test
    public void foundPathFOrAdmin() throws Exception {
        mockMvc.perform(get("/admin"))
                .andExpect(status().isFound());
    }

    @Test
    public void foundPathForUser() throws Exception {
        mockMvc.perform(get("/user"))
                .andExpect(status().isFound());
    }
}