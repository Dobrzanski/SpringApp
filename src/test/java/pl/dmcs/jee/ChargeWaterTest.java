package pl.dmcs.jee;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import pl.dmcs.jee.dao.ChargeDaoImpl;
import pl.dmcs.jee.domain.ChargeWater;

import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations={"classpath:/META-INF/root-context.xml",
        "classpath:/META-INF/servlet-context.xml"})
@Transactional
@TransactionConfiguration(defaultRollback=true)
public class ChargeWaterTest {

    @Autowired
    private ChargeDaoImpl dao;

    @Test
    public void ChargeWaterTest() {

        ChargeWater cW = dao.findChargeWaterById(2);

        double initialStateCold = cW.getInitialStateCold();
        double finalStateCold = 23.15;
        double initialStateHot = cW.getInitialStateHot();
        double finalStateHot = 85;
        double pricePerUnitCold = cW.getPricePerUnitCold();
        double pricePerUnitHot = cW.getPricePerUnitHot();

        double amountToPay = pricePerUnitCold * (finalStateCold - initialStateCold)
                + pricePerUnitHot * (finalStateHot - initialStateHot);

        amountToPay = Math.round(amountToPay * 100)/100D;

        assertTrue("Wymagany kwota do zap�aty: 33.22", 33.22 == amountToPay);

        cW.setFinalStateCold(finalStateCold);
        cW.setFinalStateHot(finalStateHot);
        cW.setAmountToPay(amountToPay);

        dao.payForChargeWater(cW);

        assertTrue("Wymagany kwota do zap�aty: 33.22", 33.22 == cW.getAmountToPay());
    }
}
