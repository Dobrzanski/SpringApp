package pl.dmcs.jee;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

import pl.dmcs.jee.configurations.Mail;

@RunWith(MockitoJUnitRunner.class)
public class MailTest {
	
	@Mock
    private MailSender mailSender;
	
	@InjectMocks
	private Mail mail;
	
	@Test
    public void testSendEmail() {
		
        String from = "spoldzielniajee@gmail.com";
        String to = "test@test.pl";
        String text = "To jest testowa tresc maila!";
        String subject = "Testowy mail";
        
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(from);
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        
        mail.sendMail(from, to, subject, text);
        
        Mockito.verify(mailSender).send(message); 
	}
}
