package pl.dmcs.jee;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import pl.dmcs.jee.dao.UserDaoImpl;
import pl.dmcs.jee.domain.User;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations={"classpath:/META-INF/root-context.xml",
        "classpath:/META-INF/servlet-context.xml"})
@Transactional
@TransactionConfiguration(defaultRollback=true)
public class UserTest {

    @Autowired
    private UserDaoImpl dao;

    @Test
    public void UserTest() {

        User user = dao.findUserById(1);

        assertTrue("Wymagane imi�: Dawid", user.getFirstname().equals("Dawid"));
        assertTrue("Wymagane nazwisko: Dobrzanski", user.getSurname().equals("Dobrzanski"));
        assertTrue("Wymagany mail: dawid8493@gmail.com",user.getMail().equals("dawid8493@gmail.com"));
        assertTrue("Wymagane miasto: Pabianice", user.getCity().equals("Pabianice"));
        assertTrue("Wymagana ulica: Zamkowa", user.getStreet().equals("Zamkowa"));
        assertTrue("Wymagany numer budynku: 1", 1 == user.getBuildingNumber());
        assertTrue("Wymagany numer lokalu: 34", 34 == user.getFlatNumber());

        User user2 = dao.findUserByMail("tester2@test.pl");

        user2.setStateColdWater(18.7);

        assertEquals(18.7, user2.getStateColdWater(), 0);

        User user3 = new User();

        user3.setCity("Wroc�aw");
        user3.setStreet("Moniuszki");
        user3.setBuildingNumber(2);
        user3.setFlatNumber(18);
        user3.setFirstname("Karol");
        user3.setSurname("jUnit");
        user3.setPhone("539-223-134");
        user3.setMail("junit@junit.pl");
        user3.setPassword("password");
        user3.setPeopleNumber(2);
        user3.setStateElectricity(0.0);
        user3.setStateGas(0.0);
        user3.setStateHeating(0.0);
        user3.setStateSevage(0.0);
        user3.setStateColdWater(0.0);
        user3.setStateHotWater(0.0);

        dao.addUser(user3);

        User user4 = dao.findUserByMail("junit@junit.pl");

        assertEquals("Wymagany numer mieszkania: 18", user4.getFlatNumber().longValue(), user3.getFlatNumber().longValue());
    }
}
