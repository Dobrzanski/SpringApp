package pl.dmcs.jee.dao;

import java.util.List;

import pl.dmcs.jee.domain.User;
import pl.dmcs.jee.domain.UserRole;

public interface UserDao {

	public List<User> getAllUsers();
	public User findUserById(Integer userId);
	public User findUserByMail(String mail);
	public void addUser(User user);
	public void editUser(User user);
	public void removeUser(Integer userId);
	public UserRole getUserRole(int id);
	public UserRole findRoleByName(String role);
}	
