package pl.dmcs.jee.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import pl.dmcs.jee.domain.Building;

@Repository("buildingDao")
public class BuildingDaoImpl extends AbstractDao<Integer, Building> implements BuildingDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<Building> getAllBuildings() {
		Criteria criteria = createEntityCriteria();
        return (List<Building>) criteria.list();
	}

	@Override
	public Building findBuilding(Integer buildingId) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("buildingId", buildingId));
		return (Building) criteria.uniqueResult();
	}
	
	@Override
	public Building findBuilding(String city, String street, Integer number) {
		Criteria criteria = createEntityCriteria();
		criteria.
		add(Restrictions.eq("city", city)).
		add(Restrictions.eq("street", street)).
		add(Restrictions.eqOrIsNull("number", number));
		
		return (Building) criteria.uniqueResult();
	}
	
	@Override
	public void addBuilding(Building building) {
		persist(building);
	}

	@Override
	public void removeBuilding(Integer buildingId) {
		Query query = getSession().createSQLQuery("delete from Building where buildingId = :buildingId");
		query.setInteger("buildingId", buildingId);
		query.executeUpdate();
	}

}
