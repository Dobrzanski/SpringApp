package pl.dmcs.jee.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import pl.dmcs.jee.domain.User;
import pl.dmcs.jee.domain.UserRole;

@Repository("userDao")
public class UserDaoImpl extends AbstractDao<Integer, User> implements UserDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<User> getAllUsers() {
		Criteria criteria = createEntityCriteria();
		return (List<User>) criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
	}

	@Override
	public User findUserById(Integer userId) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("userId", userId));
		return (User) criteria.uniqueResult();
	}

	@Override
	public User findUserByMail(String mail) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("mail", mail));
		return (User) criteria.uniqueResult();
	}

	@Override
	public void addUser(User user) {
		getSession().save(user);
	}
	
	@Override
	public void editUser(User user) {
    	getSession().merge(user);
    }

	@Override
	public void removeUser(Integer userId) {
		User user = (User) getSession().load(User.class, userId);
        if (null != user) {
            getSession().delete(user);
        }
	}
	
	public UserRole getUserRole(int id) {
		return (UserRole)getSession().get(UserRole.class, id);
	}
	
	@SuppressWarnings("unchecked")
    public UserRole findRoleByName(String role)
    {

		List<UserRole> userRole = new ArrayList<UserRole>();
	  	  
	  	userRole = getSession().createQuery("from UserRole where role=?").setParameter(0, role).list();
	
	  	if (userRole.size() > 0) {
	  		return userRole.get(0);
	  	} else {
	  		return null;
	  	}
    }
}
