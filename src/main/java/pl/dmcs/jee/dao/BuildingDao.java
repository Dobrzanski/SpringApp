package pl.dmcs.jee.dao;

import java.util.List;

import pl.dmcs.jee.domain.Building;

public interface BuildingDao {
	
	List<Building> getAllBuildings();
	Building findBuilding(Integer buildingId);
	Building findBuilding(String city, String street, Integer number);
	void addBuilding(Building building);
	void removeBuilding(Integer buildingId);
}
