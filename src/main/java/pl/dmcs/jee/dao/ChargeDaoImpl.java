package pl.dmcs.jee.dao;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import pl.dmcs.jee.domain.ChargeElectricity;
import pl.dmcs.jee.domain.ChargeGarbage;
import pl.dmcs.jee.domain.ChargeGas;
import pl.dmcs.jee.domain.ChargeHeating;
import pl.dmcs.jee.domain.ChargeOther;
import pl.dmcs.jee.domain.ChargeSevage;
import pl.dmcs.jee.domain.ChargeWater;

@Repository("chargeDao")
public class ChargeDaoImpl implements ChargeDao {

	@Autowired
    SessionFactory sessionFactory;
	
	@Override
	public ChargeElectricity findChargeElectricityById(Integer chargeId) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ChargeElectricity.class);
		criteria.add(Restrictions.eq("chargeId", chargeId));
		return (ChargeElectricity) criteria.uniqueResult();
	}

	@Override
	public ChargeGarbage findChargeGarbageById(Integer chargeId) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ChargeGarbage.class);
		criteria.add(Restrictions.eq("chargeId", chargeId));
		return (ChargeGarbage) criteria.uniqueResult();
	}

	@Override
	public ChargeGas findChargeGasById(Integer chargeId) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ChargeGas.class);
		criteria.add(Restrictions.eq("chargeId", chargeId));
		return (ChargeGas) criteria.uniqueResult();
	}

	@Override
	public ChargeHeating findChargeHeatingById(Integer chargeId) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ChargeHeating.class);
		criteria.add(Restrictions.eq("chargeId", chargeId));
		return (ChargeHeating) criteria.uniqueResult();
	}

	@Override
	public ChargeOther findChargeOtherById(Integer chargeId) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ChargeOther.class);
		criteria.add(Restrictions.eq("chargeId", chargeId));
		return (ChargeOther) criteria.uniqueResult();
	}

	@Override
	public ChargeSevage findChargeSevageById(Integer chargeId) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ChargeSevage.class);
		criteria.add(Restrictions.eq("chargeId", chargeId));
		return (ChargeSevage) criteria.uniqueResult();
	}

	@Override
	public ChargeWater findChargeWaterById(Integer chargeId) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(ChargeWater.class);
		criteria.add(Restrictions.eq("chargeId", chargeId));
		return (ChargeWater) criteria.uniqueResult();
	}

	@Override
	public void payForChargeElectricity(ChargeElectricity chargeElectricity) {
		sessionFactory.getCurrentSession().update(chargeElectricity);
	}

	@Override
	public void payForChargeGarbage(ChargeGarbage chargeGarbage) {
		sessionFactory.getCurrentSession().update(chargeGarbage);
	}

	@Override
	public void payForChargeGas(ChargeGas chargeGas) {
		sessionFactory.getCurrentSession().update(chargeGas);
	}

	@Override
	public void payForChargeHeating(ChargeHeating chargeHeating) {
		sessionFactory.getCurrentSession().update(chargeHeating);
	}

	@Override
	public void payForChargeOther(ChargeOther chargeOther) {
		sessionFactory.getCurrentSession().update(chargeOther);
	}

	@Override
	public void payForChargeSevage(ChargeSevage chargeSevage) {
		sessionFactory.getCurrentSession().update(chargeSevage);
	}

	@Override
	public void payForChargeWater(ChargeWater chargeWater) {
		sessionFactory.getCurrentSession().update(chargeWater);
	}
	
}
