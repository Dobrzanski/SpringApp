package pl.dmcs.jee.validators;

import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import pl.dmcs.jee.domain.ChargeElectricity;
import pl.dmcs.jee.domain.ChargeGarbage;
import pl.dmcs.jee.domain.ChargeGas;
import pl.dmcs.jee.domain.ChargeHeating;
import pl.dmcs.jee.domain.ChargeOther;
import pl.dmcs.jee.domain.ChargeSevage;
import pl.dmcs.jee.domain.ChargeWater;

public class ChargeValidator implements Validator {	
	  @Override
	  public boolean supports(Class clazz)
	  {
	    return ChargeElectricity.class.isAssignableFrom(clazz);
	  }
	  @Override
	  public void validate(Object arg0, Errors arg1)
	  {
	    // TODO Auto-generated method stub
	  }
	  public void validateChargeElectricity(ChargeElectricity chargeElectricity, Errors errors)
	  {
		ValidationUtils.rejectIfEmpty(errors,"captchaCode", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"name", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"paymentDate", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"finalState", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"amountToPay", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"pricePerUnit", "error.field.required");
		
		  
		if (StringUtils.hasText(String.valueOf(chargeElectricity.getFinalState()))) {
			String finalState = String.valueOf(chargeElectricity.getFinalState());
			if(finalState.equals("0.0")) {
				errors.rejectValue("finalState", "error.field.invalid");
			}
		}
	  }
	  
	  public void validateChargeGarbage(ChargeGarbage chargeGarbage, Errors errors)
	  {
		ValidationUtils.rejectIfEmpty(errors,"captchaCode", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"name", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"paymentDate", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"pricePerPerson", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"amountToPay", "error.field.required");
	  }
	  
	  public void validateChargeGas(ChargeGas chargeGas, Errors errors)
	  {
		ValidationUtils.rejectIfEmpty(errors,"captchaCode", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"name", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"paymentDate", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"pricePerUnit", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"amountToPay", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"initialState", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"finalState", "error.field.required");
		
		if (StringUtils.hasText(String.valueOf(chargeGas.getFinalState()))) {
			String finalState = String.valueOf(chargeGas.getFinalState());
			if(finalState.contains("0.0")) {
				errors.rejectValue("finalState", "error.field.invalid");
			}
		}
	  }
	  
	  public void validateChargeHeating(ChargeHeating chargeHeating, Errors errors)
	  {
		ValidationUtils.rejectIfEmpty(errors,"captchaCode", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"name", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"paymentDate", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"pricePerUnit", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"amountToPay", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"initialState", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"finalState", "error.field.required");
		
		if (StringUtils.hasText(String.valueOf(chargeHeating.getFinalState()))) {
			String finalState = String.valueOf(chargeHeating.getFinalState());
			if(finalState.contains("0.0")) {
				errors.rejectValue("finalState", "error.field.invalid");
			}
		}
	  }
	  
	  public void validateChargeOther(ChargeOther chargeOther, Errors errors)
	  {
		ValidationUtils.rejectIfEmpty(errors,"captchaCode", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"name", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"paymentDate", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"amountToPay", "error.field.required");
	  }
	  
	  public void validateChargeSevage(ChargeSevage chargeSevage, Errors errors)
	  {
		ValidationUtils.rejectIfEmpty(errors,"captchaCode", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"name", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"paymentDate", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"pricePerUnit", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"amountToPay", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"initialState", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"finalState", "error.field.required");
		
		if (StringUtils.hasText(String.valueOf(chargeSevage.getFinalState()))) {
			String finalState = String.valueOf(chargeSevage.getFinalState());
			if(finalState.contains("0.0")) {
				errors.rejectValue("finalState", "error.field.invalid");
			}
		}
	  }
	  
	  public void validateChargeWater(ChargeWater chargeWater, Errors errors)
	  {
		ValidationUtils.rejectIfEmpty(errors,"captchaCode", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"name", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"paymentDate", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"pricePerUnitCold", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"pricePerUnitHot", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"amountToPay", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"initialStateCold", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"initialStateHot", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"finalStateCold", "error.field.required");
		ValidationUtils.rejectIfEmpty(errors,"finalStateHot", "error.field.required");
		
		if (StringUtils.hasText(String.valueOf(chargeWater.getFinalStateCold()))) {
			String finalState = String.valueOf(chargeWater.getFinalStateCold());
			if(finalState.contains("0.0")) {
				errors.rejectValue("finalStateCold", "error.field.invalid");
			}
		}
		
		if (StringUtils.hasText(String.valueOf(chargeWater.getFinalStateHot()))) {
			String finalState = String.valueOf(chargeWater.getFinalStateHot());
			if(finalState.contains("0.0")) {
				errors.rejectValue("finalStateHot", "error.field.invalid");
			}
		}
	  }
}
