package pl.dmcs.jee.validators;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import pl.dmcs.jee.domain.User;

public class UserValidator implements Validator
	{
		  private int           minPasswordLength;
		  private int           maxPasswordLength;
		  private static String loginRegex             = "[a-zA-Z]*";
		  private static String digitsRegex            = ".*\\p{Digit}.*";
		  private static String specialCharactersRegex = ".*[!£$#%^&*@?<>+_].*";
		  
		  boolean isValid;
		  
		  EmailValidator emailValidator = EmailValidator.getInstance();
	  	
	  @Override
	  public boolean supports(Class clazz)
	  {
	    return User.class.isAssignableFrom(clazz);
	  }
	  @Override
	  public void validate(Object arg0, Errors arg1)
	  {
	    // TODO Auto-generated method stub
	  }
	  public void validate(User user, Errors errors)
	  {
		  ValidationUtils.rejectIfEmpty(errors,"firstname", "error.field.required");
		  ValidationUtils.rejectIfEmpty(errors,"surname", "error.field.required");
		  ValidationUtils.rejectIfEmpty(errors,"phone", "error.field.required");
		  ValidationUtils.rejectIfEmpty(errors,"mail", "error.field.required");
		  ValidationUtils.rejectIfEmpty(errors,"password", "error.field.required");
		  ValidationUtils.rejectIfEmpty(errors,"city", "error.field.required");
		  ValidationUtils.rejectIfEmpty(errors,"street", "error.field.required");
		  ValidationUtils.rejectIfEmpty(errors,"buildingNumber", "error.field.required");
		  ValidationUtils.rejectIfEmpty(errors,"flatNumber", "error.field.required");
		  ValidationUtils.rejectIfEmpty(errors,"peopleNumber", "error.field.required");
		  /*
		  ValidationUtils.rejectIfEmpty(errors,"stateColdWater", "error.field.required");
		  ValidationUtils.rejectIfEmpty(errors,"stateHotWater", "error.field.required");
		  ValidationUtils.rejectIfEmpty(errors,"stateElectricity", "error.field.required");
		  ValidationUtils.rejectIfEmpty(errors,"stateGas", "error.field.required");
		  ValidationUtils.rejectIfEmpty(errors,"stateHeating", "error.field.required");
		  ValidationUtils.rejectIfEmpty(errors,"stateSevage", "error.field.required");
		  */
		  
		  if (errors.getErrorCount() == 0) 
		  	{
		   		if (StringUtils.hasText(user.getMail()) && emailValidator.isValid(user.getMail()) == false)
		   		{
		   			errors.rejectValue("mail", "error.email.invalid");
		   		}
		   		
		   		if(StringUtils.hasText(user.getPhone()))
		   		{
		   			String phone = user.getPhone();
		   			String pattern = "(\\d{3}-)?\\d{3}-\\d{3}";
		   			isValid = phone.matches(pattern);
		   			
		   			if(isValid == false){
		   				errors.rejectValue("phone", "error.phone.invalid");
		   			}
		   		}
		  	}
	  }

	public void setMinPasswordLength(int minPasswordLength) {
		this.minPasswordLength = minPasswordLength;
	}
	public void setMaxPasswordLength(int maxPasswordLength) {
		this.maxPasswordLength = maxPasswordLength;
	}
}
