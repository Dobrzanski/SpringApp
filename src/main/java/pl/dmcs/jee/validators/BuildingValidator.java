package pl.dmcs.jee.validators;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import pl.dmcs.jee.domain.Building;

public class BuildingValidator implements Validator
	{	
		  @Override
		  public boolean supports(Class clazz)
		  {
		    return Building.class.isAssignableFrom(clazz);
		  }
		  @Override
		  public void validate(Object arg0, Errors arg1)
		  {
		    // TODO Auto-generated method stub
		  }
		  public void validate(Building building, Errors errors)
		  {
			  ValidationUtils.rejectIfEmpty(errors,"city", "error.field.required");
			  ValidationUtils.rejectIfEmpty(errors,"street", "error.field.required");
			  ValidationUtils.rejectIfEmpty(errors,"number", "error.field.required");
		  }

}
