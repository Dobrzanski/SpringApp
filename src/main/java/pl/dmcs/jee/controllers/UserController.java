package pl.dmcs.jee.controllers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.keygen.KeyGenerators;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import pl.dmcs.jee.configurations.Mail;
import pl.dmcs.jee.domain.Building;
import pl.dmcs.jee.domain.ChargeElectricity;
import pl.dmcs.jee.domain.ChargeGarbage;
import pl.dmcs.jee.domain.ChargeGas;
import pl.dmcs.jee.domain.ChargeHeating;
import pl.dmcs.jee.domain.ChargeOther;
import pl.dmcs.jee.domain.ChargeSevage;
import pl.dmcs.jee.domain.ChargeWater;
import pl.dmcs.jee.domain.User;
import pl.dmcs.jee.services.BuildingService;
import pl.dmcs.jee.services.UserService;
import pl.dmcs.jee.validators.UserValidator;

@Controller
public class UserController {
	
	@Autowired
	BuildingService buildingService;
	
	@Autowired
	UserService userService;
	
	UserValidator userValidator = new UserValidator();
	
	private List<String> uniqueCities = new ArrayList<String>();
	private List<String> uniqueStreets = new ArrayList<String>();
	private List<Integer> uniqueNumbers = new ArrayList<Integer>();
	
	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public ModelAndView forbidden403() {
 
		ModelAndView model = new ModelAndView();
		model.setViewName("403");
 
		return model; 
	}
	
	@RequestMapping(value = "/admin/user/list", method = RequestMethod.GET)
	public String allUsers(Model model) {

		List<User> users = userService.getAllUsers();
		model.addAttribute("users", users);
		return "users";
	}
	
	@RequestMapping(value = "/admin/user/add", method = RequestMethod.GET)
	public String addUser(Model model) {

		User user = new User();
		model.addAttribute("user", user);
		
		List<Building> buildings = buildingService.getAllBuildings();
		
		for(Building b: buildings) {
			if(!(uniqueCities.contains(b.getCity()))) {
				uniqueCities.add(b.getCity());
			}
			if(!(uniqueStreets.contains(b.getStreet()))) {
				uniqueStreets.add(b.getStreet());
			}
			if(!(uniqueNumbers.contains(b.getNumber()))) {
				uniqueNumbers.add(b.getNumber());
			}
		}
		Collections.sort(uniqueCities);
		Collections.sort(uniqueStreets);
		Collections.sort(uniqueNumbers);
		
		String randomPassword = KeyGenerators.string().generateKey();
		
		model.addAttribute("uniqueCities", uniqueCities);
		model.addAttribute("uniqueStreets", uniqueStreets);
		model.addAttribute("uniqueNumbers", uniqueNumbers);
		model.addAttribute("randomPassword", randomPassword);
		
		return "addUser";
	}
	
	@RequestMapping(value = "/admin/user/add", method = RequestMethod.POST)
	public String saveUser(@Valid User user, BindingResult result, 
			Model model, RedirectAttributes redirectAttributes) {
		
		List<Building> buildings = buildingService.getAllBuildings();
		
		ApplicationContext context = new ClassPathXmlApplicationContext("mail.xml");
	    	 
	    Mail mail = (Mail) context.getBean("mailMail");
		
		for(Building b: buildings) {
			if(!(uniqueCities.contains(b.getCity()))) {
				uniqueCities.add(b.getCity());
			}
			if(!(uniqueStreets.contains(b.getStreet()))) {
				uniqueStreets.add(b.getStreet());
			}
			if(!(uniqueNumbers.contains(b.getNumber()))) {
				uniqueNumbers.add(b.getNumber());
			}
		}
		Collections.sort(uniqueCities);
		Collections.sort(uniqueStreets);
		Collections.sort(uniqueNumbers);
		
		model.addAttribute("uniqueCities", uniqueCities);
		model.addAttribute("uniqueStreets", uniqueStreets);
		model.addAttribute("uniqueNumbers", uniqueNumbers);
		
		userValidator.validate(user, result);
		
		if (result.hasErrors()) {
			return "addUser";
		}
		
		if(!(userService.isUserUnique(user.getMail()))){
			model.addAttribute("flag", true);
			return "addUser";
		} else {
		
		    mail.sendMail("spoldzielniajee@gmail.com",
		    		   user.getMail(),
		    		   "Potwierdzenie utworzenia konta.", 
		    		   "Witaj " + user.getFirstname() + " " 
		    		   + user.getSurname() + "\n\nTwoje hasło to: " 
		    				   + user.getPassword());
		    
		    userService.addUser(user);
		    
			redirectAttributes.addFlashAttribute("success", "Pomyślnie dodano użytkownika!");
			redirectAttributes.addFlashAttribute("flag", true);
			
			return "redirect:/admin/user/list";
		}
	}
	
	@RequestMapping(value = "/admin/user/edit/{userId}", method = RequestMethod.GET)
	public String editUserByAdmin(@PathVariable Integer userId, Model model) {
		User user = userService.findUserById(userId);
		
		List<Building> buildings = buildingService.getAllBuildings();
		
		for(Building b: buildings) {
			if(!(uniqueCities.contains(b.getCity()))) {
				uniqueCities.add(b.getCity());
			}
			if(!(uniqueStreets.contains(b.getStreet()))) {
				uniqueStreets.add(b.getStreet());
			}
			if(!(uniqueNumbers.contains(b.getNumber()))) {
				uniqueNumbers.add(b.getNumber());
			}
		}
		Collections.sort(uniqueCities);
		Collections.sort(uniqueStreets);
		Collections.sort(uniqueNumbers);
		
		model.addAttribute("uniqueCities", uniqueCities);
		model.addAttribute("uniqueStreets", uniqueStreets);
		model.addAttribute("uniqueNumbers", uniqueNumbers);
		
		user.setPassword(null);
		
		model.addAttribute("user", user);
		model.addAttribute("edit", true);
		return "addUser";
	}
	
	@RequestMapping(value = "/admin/user/edit/{userId}", method = RequestMethod.POST)
	public String updateUserByAdmin(@Valid User user, BindingResult result, 
			Model model, RedirectAttributes redirectAttributes) {
		
		List<Building> buildings = buildingService.getAllBuildings();
		
		for(Building b: buildings) {
			if(!(uniqueCities.contains(b.getCity()))) {
				uniqueCities.add(b.getCity());
			}
			if(!(uniqueStreets.contains(b.getStreet()))) {
				uniqueStreets.add(b.getStreet());
			}
			if(!(uniqueNumbers.contains(b.getNumber()))) {
				uniqueNumbers.add(b.getNumber());
			}
		}
		Collections.sort(uniqueCities);
		Collections.sort(uniqueStreets);
		Collections.sort(uniqueNumbers);
		
		model.addAttribute("uniqueCities", uniqueCities);
		model.addAttribute("uniqueStreets", uniqueStreets);
		model.addAttribute("uniqueNumbers", uniqueNumbers);
		
		userValidator.validate(user, result);
		
		if (result.hasErrors()) {
			return "addUser";
		}
		
//		if(!(userService.isUserUnique(user.getMail()))){
//			model.addAttribute("flag", true);
//			return "addUser";
//		} else {
			userService.editUser(user);
	
			redirectAttributes.addFlashAttribute("success", "Pomyślnie zaktualizowano dane użytkownika!");
			redirectAttributes.addFlashAttribute("flag", true);
			
			return "redirect:/admin/user/list";
//		}
	}
	
	@RequestMapping(value = "/admin/user/delete/{userId}", method = RequestMethod.GET)
	public String removeUser(RedirectAttributes redirectAttributes, @PathVariable Integer userId) {
		userService.removeUser(userId);
		redirectAttributes.addFlashAttribute("success", "Pomyślnie usunięto użytkownika!");
		redirectAttributes.addFlashAttribute("flag", true);
		return "redirect:/admin/user/list";
	}
	
	@RequestMapping(value = "/user/edit/{userId}", method = RequestMethod.GET)
	public String editUserByUser(@PathVariable Integer userId, Model model) {
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String userName = auth.getName();
	    Integer loginId = userService.findUserByMail(userName).getUserId();
	    
	    if(userId != loginId) {
	    	return "redirect:/403";
	    }
	    
		User user = userService.findUserById(userId);
	    
		List<Building> buildings = buildingService.getAllBuildings();
		
		for(Building b: buildings) {
			if(!(uniqueCities.contains(b.getCity()))) {
				uniqueCities.add(b.getCity());
			}
			if(!(uniqueStreets.contains(b.getStreet()))) {
				uniqueStreets.add(b.getStreet());
			}
			if(!(uniqueNumbers.contains(b.getNumber()))) {
				uniqueNumbers.add(b.getNumber());
			}
		}
		Collections.sort(uniqueCities);
		Collections.sort(uniqueStreets);
		Collections.sort(uniqueNumbers);
		
		model.addAttribute("uniqueCities", uniqueCities);
		model.addAttribute("uniqueStreets", uniqueStreets);
		model.addAttribute("uniqueNumbers", uniqueNumbers);
		
		user.setPassword(null);
		
		model.addAttribute("user", user);
		model.addAttribute("edit", true);
		model.addAttribute("disabled", true);
		return "addUser";
	}
	
	@RequestMapping(value = "/user/edit/{userId}", method = RequestMethod.POST)
	public String updateUserByUser(@Valid User user, BindingResult result, 
			Model model, RedirectAttributes redirectAttributes) {
		
		List<Building> buildings = buildingService.getAllBuildings();
		
		for(Building b: buildings) {
			if(!(uniqueCities.contains(b.getCity()))) {
				uniqueCities.add(b.getCity());
			}
			if(!(uniqueStreets.contains(b.getStreet()))) {
				uniqueStreets.add(b.getStreet());
			}
			if(!(uniqueNumbers.contains(b.getNumber()))) {
				uniqueNumbers.add(b.getNumber());
			}
		}
		Collections.sort(uniqueCities);
		Collections.sort(uniqueStreets);
		Collections.sort(uniqueNumbers);
		
		model.addAttribute("uniqueCities", uniqueCities);
		model.addAttribute("uniqueStreets", uniqueStreets);
		model.addAttribute("uniqueNumbers", uniqueNumbers);
		
		userValidator.validate(user, result);
		
		if (result.hasErrors()) {
			return "addUser";
		}
		
//		if(!(userService.isUserUnique(user.getMail()))){
//			model.addAttribute("flag", true);
//			return "addUser";
//		} else {
			userService.editUser(user);
			
			model.addAttribute("success", "Pomyślnie zaktualizowano dane użytkownika!");
			model.addAttribute("flag", true);
			return "start";
//		}
	}
}
