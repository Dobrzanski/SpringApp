package pl.dmcs.jee.controllers;

import com.captcha.botdetect.web.servlet.Captcha;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.dmcs.jee.domain.*;
import pl.dmcs.jee.services.ChargeService;
import pl.dmcs.jee.services.ReportService;
import pl.dmcs.jee.services.UserService;
import pl.dmcs.jee.validators.ChargeValidator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
public class ChargeController {
	
	@Autowired
	ChargeService chargeService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	ReportService reportService;
	
	ChargeValidator chargeValidator = new ChargeValidator();
	
	@RequestMapping(value = "/user/charge/list/{userId}", method = RequestMethod.GET)
	public String getChargesForUser(@PathVariable Integer userId, Model model) {
	    
		User user = userService.findUserById(userId);
		
		ChargeElectricity chargeElectricity;
		ChargeGarbage chargeGarbage;
		ChargeGas chargeGas;
		ChargeHeating chargeHeating;
		ChargeOther chargeOther;
		ChargeSevage chargeSevage;
		ChargeWater chargeWater;
		
		List<ChargeElectricity> listChargeElectricity = new ArrayList<ChargeElectricity>();
		List<ChargeGarbage> listChargeGarbage = new ArrayList<ChargeGarbage>();
		List<ChargeGas> listChargeGas = new ArrayList<ChargeGas>();
		List<ChargeHeating> listChargeHeating = new ArrayList<ChargeHeating>();
		List<ChargeOther> listChargeOther = new ArrayList<ChargeOther>();
		List<ChargeSevage> listChargeSevage = new ArrayList<ChargeSevage>();
		List<ChargeWater> listChargeWater = new ArrayList<ChargeWater>();
		
		Iterator iteratorChargeElectricity = user.getChargeElectricity().iterator();
		Iterator iteratorChargeGarbage = user.getChargeGarbage().iterator();
		Iterator iteratorChargeGas = user.getChargeGas().iterator();
		Iterator iteratorChargeHeating = user.getChargeHeating().iterator();
		Iterator iteratorChargeOther = user.getChargeOther().iterator();
		Iterator iteratorChargeSevage = user.getChargeSevage().iterator();
		Iterator iteratorChargeWater = user.getChargeWater().iterator();
		
		while(iteratorChargeElectricity.hasNext())
		{
			chargeElectricity = ((ChargeElectricity)iteratorChargeElectricity.next());
			listChargeElectricity.add(chargeElectricity);
		}
		while(iteratorChargeGarbage.hasNext())
		{
			chargeGarbage = ((ChargeGarbage)iteratorChargeGarbage.next());
			listChargeGarbage.add(chargeGarbage);
		}
		while(iteratorChargeGas.hasNext())
		{
			chargeGas = ((ChargeGas)iteratorChargeGas.next());
			listChargeGas.add(chargeGas);
		}
		while(iteratorChargeHeating.hasNext())
		{
			chargeHeating = ((ChargeHeating)iteratorChargeHeating.next());
			listChargeHeating.add(chargeHeating);
		}
		while(iteratorChargeOther.hasNext())
		{
			chargeOther = ((ChargeOther)iteratorChargeOther.next());
			listChargeOther.add(chargeOther);
		}
		while(iteratorChargeSevage.hasNext())
		{
			chargeSevage = ((ChargeSevage)iteratorChargeSevage.next());
			listChargeSevage.add(chargeSevage);
		}
		while(iteratorChargeWater.hasNext())
		{
			chargeWater = ((ChargeWater)iteratorChargeWater.next());
			listChargeWater.add(chargeWater);
		}
		
		model.addAttribute("listChargeElectricity", listChargeElectricity);
		model.addAttribute("listChargeGarbage", listChargeGarbage);
		model.addAttribute("listChargeGas", listChargeGas);
		model.addAttribute("listChargeHeating", listChargeHeating);
		model.addAttribute("listChargeOther", listChargeOther);
		model.addAttribute("listChargeSevage", listChargeSevage);
		model.addAttribute("listChargeWater", listChargeWater);
		
		return "charges";
	}
	
	@RequestMapping(value = "admin/charge/list", method = RequestMethod.GET)
	public String getAllCharges(Model model) {
	    
		List<User> allUsers = userService.getAllUsers();
		List<User> listUsersWithChargesElectricity = new ArrayList<User>();
		List<User> listUsersWithChargesGarbage = new ArrayList<User>();
		List<User> listUsersWithChargesGas = new ArrayList<User>();
		List<User> listUsersWithChargesHeating = new ArrayList<User>();
		List<User> listUsersWithChargesOther = new ArrayList<User>();
		List<User> listUsersWithChargesSevage = new ArrayList<User>();
		List<User> listUsersWithChargesWater = new ArrayList<User>();
		
		ChargeElectricity chargeElectricity;
		ChargeGarbage chargeGarbage;
		ChargeGas chargeGas;
		ChargeHeating chargeHeating;
		ChargeOther chargeOther;
		ChargeSevage chargeSevage;
		ChargeWater chargeWater;
		
		List<ChargeElectricity> listChargeElectricity = new ArrayList<ChargeElectricity>();
		List<ChargeGarbage> listChargeGarbage = new ArrayList<ChargeGarbage>();
		List<ChargeGas> listChargeGas = new ArrayList<ChargeGas>();
		List<ChargeHeating> listChargeHeating = new ArrayList<ChargeHeating>();
		List<ChargeOther> listChargeOther = new ArrayList<ChargeOther>();
		List<ChargeSevage> listChargeSevage = new ArrayList<ChargeSevage>();
		List<ChargeWater> listChargeWater = new ArrayList<ChargeWater>();
		
		for(User u: allUsers) {
			
			Iterator iteratorChargeElectricity = u.getChargeElectricity().iterator();
			Iterator iteratorChargeGarbage = u.getChargeGarbage().iterator();
			Iterator iteratorChargeGas = u.getChargeGas().iterator();
			Iterator iteratorChargeHeating = u.getChargeHeating().iterator();
			Iterator iteratorChargeOther = u.getChargeOther().iterator();
			Iterator iteratorChargeSevage = u.getChargeSevage().iterator();
			Iterator iteratorChargeWater = u.getChargeWater().iterator();
			
			while(iteratorChargeElectricity.hasNext())
			{
				chargeElectricity = ((ChargeElectricity)iteratorChargeElectricity.next());
				listChargeElectricity.add(chargeElectricity);
				listUsersWithChargesElectricity.add(u);
			}
			while(iteratorChargeGarbage.hasNext())
			{
				chargeGarbage = ((ChargeGarbage)iteratorChargeGarbage.next());
				listChargeGarbage.add(chargeGarbage);
				listUsersWithChargesGarbage.add(u);
			}
			while(iteratorChargeGas.hasNext())
			{
				chargeGas = ((ChargeGas)iteratorChargeGas.next());
				listChargeGas.add(chargeGas);
				listUsersWithChargesGas.add(u);
			}
			while(iteratorChargeHeating.hasNext())
			{
				chargeHeating = ((ChargeHeating)iteratorChargeHeating.next());
				listChargeHeating.add(chargeHeating);
				listUsersWithChargesHeating.add(u);
			}
			while(iteratorChargeOther.hasNext())
			{
				chargeOther = ((ChargeOther)iteratorChargeOther.next());
				listChargeOther.add(chargeOther);
				listUsersWithChargesOther.add(u);
			}
			while(iteratorChargeSevage.hasNext())
			{
				chargeSevage = ((ChargeSevage)iteratorChargeSevage.next());
				listChargeSevage.add(chargeSevage);
				listUsersWithChargesSevage.add(u);
			}
			while(iteratorChargeWater.hasNext())
			{
				chargeWater = ((ChargeWater)iteratorChargeWater.next());
				listChargeWater.add(chargeWater);
				listUsersWithChargesWater.add(u);
			}
			
		}
		
		model.addAttribute("listChargeElectricity", listChargeElectricity);
		model.addAttribute("listChargeGarbage", listChargeGarbage);
		model.addAttribute("listChargeGas", listChargeGas);
		model.addAttribute("listChargeHeating", listChargeHeating);
		model.addAttribute("listChargeOther", listChargeOther);
		model.addAttribute("listChargeSevage", listChargeSevage);
		model.addAttribute("listChargeWater", listChargeWater);
		model.addAttribute("listUsersWithChargesElectricity", listUsersWithChargesElectricity);
		model.addAttribute("listUsersWithChargesGarbage", listUsersWithChargesGarbage);
		model.addAttribute("listUsersWithChargesGas", listUsersWithChargesGas);
		model.addAttribute("listUsersWithChargesHeating", listUsersWithChargesHeating);
		model.addAttribute("listUsersWithChargesOther", listUsersWithChargesOther);
		model.addAttribute("listUsersWithChargesSevage", listUsersWithChargesSevage);
		model.addAttribute("listUsersWithChargesWater", listUsersWithChargesWater);
		
		return "allCharges";
	}
	
	@RequestMapping(value = "/admin/charge/new", method = RequestMethod.GET)
	public String newCharge(Model model) {

		return "newCharge";
	}
	
	@RequestMapping(value = "/admin/charge/add/electricity", method = RequestMethod.GET)
	public String addChargeElectricity(Model model) {

		ChargeElectricity chargeElectricity = new ChargeElectricity();
		model.addAttribute("chargeElectricity", chargeElectricity);
		return "addChargeElectricity";
	}
	
	@RequestMapping(value = "/admin/charge/add/electricity", method = RequestMethod.POST)
	public String calculateChargeElectricity(@Valid ChargeElectricity chargeElectricity, BindingResult result, 
			Model model, RedirectAttributes redirectAttributes) {
		    
		    chargeService.calculateChargeElectricity(chargeElectricity);
			
			return "newCharge";
	}
	
	@RequestMapping(value = "/admin/charge/add/garbage", method = RequestMethod.GET)
	public String addChargeGarbage(Model model) {

		ChargeGarbage chargeGarbage = new ChargeGarbage();
		model.addAttribute("chargeGarbage", chargeGarbage);
		return "addChargeGarbage";
	}
	
	@RequestMapping(value = "/admin/charge/add/garbage", method = RequestMethod.POST)
	public String calculateChargeGarbage(@Valid ChargeGarbage chargeGarbage, BindingResult result, 
			Model model, RedirectAttributes redirectAttributes) {
		    
		    chargeService.calculateChargeGarbage(chargeGarbage);
			
			return "newCharge";
	}
	
	@RequestMapping(value = "/admin/charge/add/gas", method = RequestMethod.GET)
	public String addChargeGas(Model model) {

		ChargeGas chargeGas = new ChargeGas();
		model.addAttribute("chargeGas", chargeGas);
		return "addChargeGas";
	}
	
	@RequestMapping(value = "/admin/charge/add/gas", method = RequestMethod.POST)
	public String calculateChargeGas(@Valid ChargeGas chargeGas, BindingResult result, 
			Model model, RedirectAttributes redirectAttributes) {
		    
		    chargeService.calculateChargeGas(chargeGas);
			
			return "newCharge";
	}
	
	@RequestMapping(value = "/admin/charge/add/heating", method = RequestMethod.GET)
	public String addChargeHeating(Model model) {

		ChargeHeating chargeHeating = new ChargeHeating();
		model.addAttribute("chargeHeating", chargeHeating);
		return "addChargeHeating";
	}
	
	@RequestMapping(value = "/admin/charge/add/heating", method = RequestMethod.POST)
	public String calculateChargeHeating(@Valid ChargeHeating chargeHeating, BindingResult result, 
			Model model, RedirectAttributes redirectAttributes) {
		    
		    chargeService.calculateChargeHeating(chargeHeating);
			
			return "newCharge";
	}
	
	@RequestMapping(value = "/admin/charge/add/other", method = RequestMethod.GET)
	public String addChargeOther(Model model) {

		ChargeOther chargeOther = new ChargeOther();
		model.addAttribute("chargeOther", chargeOther);
		return "addChargeOther";
	}
	
	@RequestMapping(value = "/admin/charge/add/other", method = RequestMethod.POST)
	public String calculateChargeOther(@Valid ChargeOther chargeOther, BindingResult result, 
			Model model, RedirectAttributes redirectAttributes) {
		    
		    chargeService.calculateChargeOther(chargeOther);
			
			return "newCharge";
	}
	
	@RequestMapping(value = "/admin/charge/add/sevage", method = RequestMethod.GET)
	public String addChargeSevage(Model model) {

		ChargeSevage chargeSevage = new ChargeSevage();
		model.addAttribute("chargeSevage", chargeSevage);
		return "addChargeSevage";
	}
	
	@RequestMapping(value = "/admin/charge/add/sevage", method = RequestMethod.POST)
	public String calculateChargeSevage(@Valid ChargeSevage chargeSevage, BindingResult result, 
			Model model, RedirectAttributes redirectAttributes) {
		    
		    chargeService.calculateChargeSevage(chargeSevage);
			
			return "newCharge";
	}
	
	@RequestMapping(value = "/admin/charge/add/water", method = RequestMethod.GET)
	public String addChargeWater(Model model) {

		ChargeWater chargeWater = new ChargeWater();
		model.addAttribute("chargeWater", chargeWater);
		return "addChargeWater";
	}
	
	@RequestMapping(value = "/admin/charge/add/water", method = RequestMethod.POST)
	public String calculateChargeWater(@Valid ChargeWater chargeWater, BindingResult result, 
			Model model, RedirectAttributes redirectAttributes) {
		    
		    chargeService.calculateChargeWater(chargeWater);
			
			return "newCharge";
	}
	
	@RequestMapping(value = "/user/charge/pay/electricity/{chargeId}", method = RequestMethod.GET)
	public String payForChargeElectricity(@PathVariable Integer chargeId, Model model) throws ParseException {

		ChargeElectricity chargeElectricity = chargeService.findChargeElectricityById(chargeId);
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String currentDateInString = dateFormat.format(new Date());
		String paymentDateInString = chargeElectricity.getPaymentDate().toString();
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

	    Date currentDate = format.parse(currentDateInString);
	    Date paymentDate = format.parse(paymentDateInString);
		
		if(paymentDate.compareTo(currentDate) < 0) {
			model.addAttribute("lumpSum", true); //rycza�t
		} 
		model.addAttribute("chargeElectricity", chargeElectricity);
		model.addAttribute("user", true);
		
		return "addChargeElectricity";
	}
	
	@RequestMapping(value = "/user/charge/pay/electricity/{chargeId}", method = RequestMethod.POST)
	public String confirmPayForChargeElectricity(@Valid ChargeElectricity chargeElectricity, BindingResult result, 
			Model model, RedirectAttributes redirectAttributes, HttpServletRequest request) {
		
			Captcha captcha = Captcha.load(request, "exampleCaptcha");
			boolean isHuman = captcha.validate(chargeElectricity.getCaptchaCode());
			
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			String userName = authentication.getName();
			User user = userService.findUserByMail(userName);
			//Integer userId = userService.findUserByMail(userName).getUserId();
			Integer userId = user.getUserId();

			chargeValidator.validateChargeElectricity(chargeElectricity, result);
			
			if (result.hasErrors()) {
				model.addAttribute("user", true);
				return "addChargeElectricity";
			}
			
			if(isHuman) {
				user.setStateElectricity(chargeElectricity.getFinalState());
				userService.editUserWithoutPassword(user);
				chargeElectricity.setStatus(true);
			    chargeService.payForChargeElectricity(chargeElectricity);
			} else {
				model.addAttribute("user", true);
				model.addAttribute("error", "Nieudana weryfikacja!");
				chargeElectricity.setCaptchaCode("");
				chargeService.payForChargeElectricity(chargeElectricity);
				return "addChargeElectricity";
			}
			
			redirectAttributes.addAttribute("userId", userId);
			
			return "redirect:/user/charge/list/{userId}";
	}
	
	@RequestMapping(value = "/user/charge/pay/garbage/{chargeId}", method = RequestMethod.GET)
	public String payForChargeGarbage(@PathVariable Integer chargeId, Model model) {

		ChargeGarbage chargeGarbage = chargeService.findChargeGarbageById(chargeId);
		
		model.addAttribute("chargeGarbage", chargeGarbage);
		model.addAttribute("user", true);
		
		return "addChargeGarbage";
	}
	
	@RequestMapping(value = "/user/charge/pay/garbage/{chargeId}", method = RequestMethod.POST)
	public String confirmPayForChargeGarbage(@Valid ChargeGarbage chargeGarbage, BindingResult result, 
			Model model, RedirectAttributes redirectAttributes, HttpServletRequest request) {
		
			Captcha captcha = Captcha.load(request, "exampleCaptcha");
			boolean isHuman = captcha.validate(chargeGarbage.getCaptchaCode());
			
			chargeValidator.validateChargeGarbage(chargeGarbage, result);
			
			if (result.hasErrors()) {
				model.addAttribute("user", true);
				return "addChargeGarbage";
			}
			
			if(isHuman) {
				chargeGarbage.setStatus(true);
			    chargeService.payForChargeGarbage(chargeGarbage);
			} else {
				model.addAttribute("user", true);
				model.addAttribute("error", "Nieudana weryfikacja!");
				chargeGarbage.setCaptchaCode("");
				chargeService.payForChargeGarbage(chargeGarbage);
				return "addChargeGarbage";
			}
			
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			String userName = authentication.getName();
			Integer userId = userService.findUserByMail(userName).getUserId();
			
			redirectAttributes.addAttribute("userId", userId);
			
			return "redirect:/user/charge/list/{userId}";
	}
	
	@RequestMapping(value = "/user/charge/pay/gas/{chargeId}", method = RequestMethod.GET)
	public String payForChargeGas(@PathVariable Integer chargeId, Model model) throws ParseException {

		ChargeGas chargeGas = chargeService.findChargeGasById(chargeId);
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String currentDateInString = dateFormat.format(new Date());
		String paymentDateInString = chargeGas.getPaymentDate().toString();
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

	    Date currentDate = format.parse(currentDateInString);
	    Date paymentDate = format.parse(paymentDateInString);
		
		if(paymentDate.compareTo(currentDate) < 0) {
			model.addAttribute("lumpSum", true); //rycza�t
		} 
		
		model.addAttribute("chargeGas", chargeGas);
		model.addAttribute("user", true);
		
		return "addChargeGas";
	}
	
	@RequestMapping(value = "/user/charge/pay/gas/{chargeId}", method = RequestMethod.POST)
	public String confirmPayForChargeGas(@Valid ChargeGas chargeGas, BindingResult result, 
			Model model, RedirectAttributes redirectAttributes, HttpServletRequest request) {
		
			Captcha captcha = Captcha.load(request, "exampleCaptcha");
			boolean isHuman = captcha.validate(chargeGas.getCaptchaCode());
			
			chargeValidator.validateChargeGas(chargeGas, result);
			
			if (result.hasErrors()) {
				model.addAttribute("user", true);
				return "addChargeGas";
			}
			
			if(isHuman) {
				chargeGas.setStatus(true);
			    chargeService.payForChargeGas(chargeGas);
			} else {
				model.addAttribute("user", true);
				model.addAttribute("error", "Nieudana weryfikacja!");
				chargeGas.setCaptchaCode("");
				chargeService.payForChargeGas(chargeGas);
				return "addChargeGas";
			}
			
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			String userName = authentication.getName();
			Integer userId = userService.findUserByMail(userName).getUserId();
			
			redirectAttributes.addAttribute("userId", userId);
			
			return "redirect:/user/charge/list/{userId}";
	}
	
	@RequestMapping(value = "/user/charge/pay/heating/{chargeId}", method = RequestMethod.GET)
	public String payForChargeHeating(@PathVariable Integer chargeId, Model model) {

		ChargeHeating chargeHeating = chargeService.findChargeHeatingById(chargeId);
		
		model.addAttribute("chargeHeating", chargeHeating);
		model.addAttribute("user", true);
		
		return "addChargeHeating";
	}
	
	@RequestMapping(value = "/user/charge/pay/heating/{chargeId}", method = RequestMethod.POST)
	public String confirmPayForChargeHeating(@Valid ChargeHeating chargeHeating, BindingResult result, 
			Model model, RedirectAttributes redirectAttributes, HttpServletRequest request) {
		
			Captcha captcha = Captcha.load(request, "exampleCaptcha");
			boolean isHuman = captcha.validate(chargeHeating.getCaptchaCode());
			
			chargeValidator.validateChargeHeating(chargeHeating, result);
			
			if (result.hasErrors()) {
				model.addAttribute("user", true);
				return "addChargeHeating";
			}
			
			if(isHuman) {
				chargeHeating.setStatus(true);
			    chargeService.payForChargeHeating(chargeHeating);
			} else {
				model.addAttribute("user", true);
				model.addAttribute("error", "Nieudana weryfikacja!");
				chargeHeating.setCaptchaCode("");
				chargeService.payForChargeHeating(chargeHeating);
				return "addChargeHeating";
			}
			
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			String userName = authentication.getName();
			Integer userId = userService.findUserByMail(userName).getUserId();
			
			redirectAttributes.addAttribute("userId", userId);
			
			return "redirect:/user/charge/list/{userId}";
	}
	
	@RequestMapping(value = "/user/charge/pay/other/{chargeId}", method = RequestMethod.GET)
	public String payForChargeOther(@PathVariable Integer chargeId, Model model) {

		ChargeOther chargeOther = chargeService.findChargeOtherById(chargeId);
		
		model.addAttribute("chargeOther", chargeOther);
		model.addAttribute("user", true);
		
		return "addChargeOther";
	}
	
	@RequestMapping(value = "/user/charge/pay/other/{chargeId}", method = RequestMethod.POST)
	public String confirmPayForChargeOther(@Valid ChargeOther chargeOther, BindingResult result, 
			Model model, RedirectAttributes redirectAttributes, HttpServletRequest request) {
		
			Captcha captcha = Captcha.load(request, "exampleCaptcha");
			boolean isHuman = captcha.validate(chargeOther.getCaptchaCode());
			
			chargeValidator.validateChargeOther(chargeOther, result);
			
			if (result.hasErrors()) {
				model.addAttribute("user", true);
				return "addChargeOther";
			}
			
			if(isHuman) {
				chargeOther.setStatus(true);
			    chargeService.payForChargeOther(chargeOther);
			} else {
				model.addAttribute("user", true);
				model.addAttribute("error", "Nieudana weryfikacja!");
				chargeOther.setCaptchaCode("");
				chargeService.payForChargeOther(chargeOther);
				return "addChargeOther";
			}
			
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			String userName = authentication.getName();
			Integer userId = userService.findUserByMail(userName).getUserId();
			
			redirectAttributes.addAttribute("userId", userId);
			
			return "redirect:/user/charge/list/{userId}";
	}
	
	@RequestMapping(value = "/user/charge/pay/sevage/{chargeId}", method = RequestMethod.GET)
	public String payForChargeSevage(@PathVariable Integer chargeId, Model model) throws ParseException {

		ChargeSevage chargeSevage = chargeService.findChargeSevageById(chargeId);
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String currentDateInString = dateFormat.format(new Date());
		String paymentDateInString = chargeSevage.getPaymentDate().toString();
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

	    Date currentDate = format.parse(currentDateInString);
	    Date paymentDate = format.parse(paymentDateInString);
		
		if(paymentDate.compareTo(currentDate) < 0) {
			model.addAttribute("lumpSum", true); //rycza�t
		} 
		
		model.addAttribute("chargeSevage", chargeSevage);
		model.addAttribute("user", true);
		
		return "addChargeSevage";
	}
	
	@RequestMapping(value = "/user/charge/pay/sevage/{chargeId}", method = RequestMethod.POST)
	public String confirmPayForChargeSevage(@Valid ChargeSevage chargeSevage, BindingResult result, 
			Model model, RedirectAttributes redirectAttributes, HttpServletRequest request) {
		
			Captcha captcha = Captcha.load(request, "exampleCaptcha");
			boolean isHuman = captcha.validate(chargeSevage.getCaptchaCode());
			
			chargeValidator.validateChargeSevage(chargeSevage, result);
			
			if (result.hasErrors()) {
				model.addAttribute("user", true);
				return "addChargeSevage";
			}
			
			if(isHuman) {
				chargeSevage.setStatus(true);
			    chargeService.payForChargeSevage(chargeSevage);
			} else {
				model.addAttribute("user", true);
				model.addAttribute("error", "Nieudana weryfikacja!");
				chargeSevage.setCaptchaCode("");
				chargeService.payForChargeSevage(chargeSevage);
				return "addChargeSevage";
			}
			
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			String userName = authentication.getName();
			Integer userId = userService.findUserByMail(userName).getUserId();
			
			redirectAttributes.addAttribute("userId", userId);
			
			return "redirect:/user/charge/list/{userId}";
	}
	
	@RequestMapping(value = "/user/charge/pay/water/{chargeId}", method = RequestMethod.GET)
	public String payForChargeWater(@PathVariable Integer chargeId, Model model) throws ParseException {

		ChargeWater chargeWater = chargeService.findChargeWaterById(chargeId);
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String currentDateInString = dateFormat.format(new Date());
		String paymentDateInString = chargeWater.getPaymentDate().toString();
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

	    Date currentDate = format.parse(currentDateInString);
	    Date paymentDate = format.parse(paymentDateInString);
		
		if(paymentDate.compareTo(currentDate) < 0) {
			model.addAttribute("lumpSum", true); //rycza�t
		} 
		
		model.addAttribute("chargeWater", chargeWater);
		model.addAttribute("user", true);
		
		return "addChargeWater";
	}
	
	@RequestMapping(value = "/user/charge/pay/water/{chargeId}", method = RequestMethod.POST)
	public String confirmPayForChargeWater(@Valid ChargeWater chargeWater, BindingResult result, 
			Model model, RedirectAttributes redirectAttributes, HttpServletRequest request) {
		
			Captcha captcha = Captcha.load(request, "exampleCaptcha");
			boolean isHuman = captcha.validate(chargeWater.getCaptchaCode());
			
			chargeValidator.validateChargeWater(chargeWater, result);
			
			if (result.hasErrors()) {
				model.addAttribute("user", true);
				return "addChargeWater";
			}
			
			if(isHuman) {
				chargeWater.setStatus(true);
			    chargeService.payForChargeWater(chargeWater);
			} else {
				model.addAttribute("user", true);
				model.addAttribute("error", "Nieudana weryfikacja!");
				chargeWater.setCaptchaCode("");
				chargeService.payForChargeWater(chargeWater);
				return "addChargeWater";
			}
			
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			String userName = authentication.getName();
			Integer userId = userService.findUserByMail(userName).getUserId();
			
			redirectAttributes.addAttribute("userId", userId);
			
			return "redirect:/user/charge/list/{userId}";
	}
	
	@RequestMapping(value = "/generatePDF", method = RequestMethod.GET)
	public String generateMonthPDF() {
		
		reportService.generatePDF();
		
		return "redirect:/";
	}
	
	@RequestMapping(value = "/user/charge/report.html")
	public void showPdf(HttpServletRequest request, HttpServletResponse response) {
		
		int userId = ServletRequestUtils.getIntParameter(request, "userId", -1);
			
	  	if (userId<1) {
	  		return; 
	  	}
	  	
	  	User user = userService.findUserById(userId);
	  
	    if (user.getPdf() == null) {
	    	return;
	    }
	        
	    try {
	    	OutputStream o = response.getOutputStream();
			response.setCharacterEncoding("UTF-8");
			response.setContentType("application/pdf");
			response.setHeader("Content-Disposition", "inline; filename=Raport.pdf");
			if (user.getPdf() != null) {
				o.write(user.getPdf());
			}
			o.flush();
			o.close();
		} catch (IOException e) {}
	}
	
	@RequestMapping(value = "/admin/charge/yearlyReport", method = RequestMethod.GET)
	public String generateYearlyReport(Locale locale, Model model) {
		
		int year = Calendar.getInstance().get(Calendar.YEAR);
		
		List<Integer> listOfYears = new ArrayList<Integer>();
		
		for(int i = 0; i < 5; i++) {
			listOfYears.add(year - i);
		}
		
		model.addAttribute("years", listOfYears);
		
		return "yearlyReport";
	}
	
	@RequestMapping(value = "/admin/charge/yearlyReport/{year}", method = RequestMethod.GET)
	public String confirmGenerateYearlyReport(@PathVariable("year") Integer year, Locale locale, Model model) {
		
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		
		List<Integer> listOfYears = new ArrayList<Integer>();
		
		for(int i = 0; i < 5; i++) {
			listOfYears.add(currentYear - i);
		}
		
		reportService.generateYearlyPDF(year);
		
		model.addAttribute("years", listOfYears);
		
		return "yearlyReport";
	}
	
	@RequestMapping(value = "/user/charge/reportYearly.html")
	public void showPdfYearly(HttpServletRequest request, HttpServletResponse response) {
		
		int userId = ServletRequestUtils.getIntParameter(request, "userId", -1);
			
	  	if (userId<1) {
	  		return; 
	  	}
	  	
	  	User user = userService.findUserById(userId);
	  
	    if (user.getPdfYearly() == null) {
	    	return;
	    }
	        
	    try {
	    	OutputStream o = response.getOutputStream();
			response.setCharacterEncoding("UTF-8");
			response.setContentType("application/pdf");
			response.setHeader("Content-Disposition", "inline; filename=Raport.pdf");
			if (user.getPdfYearly() != null) {
				o.write(user.getPdfYearly());
			}
			o.flush();
			o.close();
		} catch (IOException e) {}
	}
}
