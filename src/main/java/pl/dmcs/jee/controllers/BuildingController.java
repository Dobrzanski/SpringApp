package pl.dmcs.jee.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import pl.dmcs.jee.domain.Building;
import pl.dmcs.jee.services.BuildingService;
import pl.dmcs.jee.validators.BuildingValidator;

@Controller
public class BuildingController {

	@Autowired
	BuildingService buildingService;
	
	BuildingValidator buildingValidator = new BuildingValidator();
	
	@RequestMapping(value = "/admin/building/list", method = RequestMethod.GET)
	public String allBuildings(Model model) {

		List<Building> buildings = buildingService.getAllBuildings();
		model.addAttribute("buildings", buildings);
		return "buildings";
	}
	
	@RequestMapping(value = "/admin/building/add", method = RequestMethod.GET)
	public String addBuilding(Model model) {
		Building building = new Building();
		model.addAttribute("building", building);
		model.addAttribute("edit", false);
		return "addBuilding";
	}
	
	@RequestMapping(value = "/admin/building/add", method = RequestMethod.POST)
	public String saveBuilding(@Valid Building building, BindingResult result, 
			Model model, RedirectAttributes redirectAttributes) {
		
		buildingValidator.validate(building, result);
		
		if (result.hasErrors()) {
			return "addBuilding";
		}
		
		if(!(buildingService.isBuildingUnique(building.getCity(), building.getStreet(), building.getNumber()))){
			model.addAttribute("flag", true);
			return "addBuilding";
		} else {
			buildingService.addBuilding(building);
	
			redirectAttributes.addFlashAttribute("success", "Pomy�lnie dodano budynek!");
			redirectAttributes.addFlashAttribute("flag", true);
			return "redirect:/admin/building/list";
		}
	}
	
	@RequestMapping(value = "/admin/building/edit/{buildingId}", method = RequestMethod.GET)
	public String editBuilding(@PathVariable Integer buildingId, Model model) {
		Building building = buildingService.findBuilding(buildingId);
		model.addAttribute("building", building);
		model.addAttribute("edit", true);
		return "addBuilding";
	}
	
	@RequestMapping(value = "/admin/building/edit/{buildingId}", method = RequestMethod.POST)
	public String updateBuilding(@Valid Building building, BindingResult result,
			Model model, RedirectAttributes redirectAttributes, @PathVariable Integer buildingId) {
		
		model.addAttribute("edit", true);
		buildingValidator.validate(building, result);
		
		if (result.hasErrors()) {
			return "addBuilding";
		}
		
		if(!(buildingService.isBuildingUnique(building.getCity(), building.getStreet(), building.getNumber()))){
			model.addAttribute("flag", true);
			return "addBuilding";
		} else {
			buildingService.updateBuilding(building);
			redirectAttributes.addFlashAttribute("success", "Pomy�lnie zaktualizowano dane budynku!");
			redirectAttributes.addFlashAttribute("flag", true);
			return "redirect:/admin/building/list";
		}
	}
	
	@RequestMapping(value = "/admin/building/delete/{buildingId}", method = RequestMethod.GET)
	public String removeBuilding(RedirectAttributes redirectAttributes, @PathVariable Integer buildingId) {
		buildingService.removeBuilding(buildingId);
		redirectAttributes.addFlashAttribute("success", "Pomy�lnie usuni�to budynek!");
		redirectAttributes.addFlashAttribute("flag", true);
		return "redirect:/admin/building/list";
	}
}
