package pl.dmcs.jee.services;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import pl.dmcs.jee.domain.ChargeElectricity;
import pl.dmcs.jee.domain.ChargeGarbage;
import pl.dmcs.jee.domain.ChargeGas;
import pl.dmcs.jee.domain.ChargeHeating;
import pl.dmcs.jee.domain.ChargeOther;
import pl.dmcs.jee.domain.ChargeSevage;
import pl.dmcs.jee.domain.ChargeWater;
import pl.dmcs.jee.domain.User;
import pl.dmcs.jee.domain.UserRole;

@Service("reportService")
@Transactional
public class ReportServiceImpl implements ReportService {
	
	@Autowired
	private UserService userService;
	
	@Scheduled(cron="0 0 0 1 * *") //1 dzie� miesi�ca, o p�nocy
	public void generatePDF() {

		DateFormat dateFormat = new SimpleDateFormat("MMMM");
		DateFormat dateFormat2 = new SimpleDateFormat("dd.MM.yyyy");
		Date date = new Date();
		String dateString = dateFormat.format(date).toString();
		
		List<User> listOfUsers = userService.getAllUsers();
		
		for(User user: listOfUsers) {
			Set<UserRole> userRoleList = user.getUserRole();
			
			for(UserRole userRole: userRoleList) {
				if(userRole.getRole().contains("ROLE_USER")) {
					
					try {
			            
			            Document document = new Document();
			            
			            ByteArrayOutputStream stream = new ByteArrayOutputStream();
			            PdfWriter.getInstance(document, stream);
			            
			            document.open();

			            Paragraph paragraph = new Paragraph("Pabianice, " 
			            + dateFormat2.format(new Date()).toString() + "r.");
			            
			            paragraph.setAlignment(Element.ALIGN_RIGHT);
			            document.add(paragraph);
			            
			            document.add(new Paragraph(""));

			            document.addTitle("Rachunek " + user.getBuildingNumber() 
			            + "#" + user.getFlatNumber() + " - " + dateString);
			
			            //Create Paragraph
			            BaseFont helvetica = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1250, BaseFont.EMBEDDED);
			            Font helvetica16 = new Font(helvetica, 16, Font.BOLD);
			            Font helvetica12Bold = new Font(helvetica, 12, Font.BOLD);
			            Font helvetica12 = new Font(helvetica, 12);
			            
			            Paragraph paragraph2 = new Paragraph("Rachunek za miesi�c: " + dateString + ".", helvetica16);
			            paragraph2.add(new Paragraph(" "));
			            paragraph2.setAlignment(Element.ALIGN_CENTER);
			            document.add(paragraph2);
			
			            //Create a table in PDF
			            PdfPTable pdfTable = new PdfPTable(4);
			            PdfPCell cell1 = new PdfPCell(new Phrase("Nazwa op�aty", helvetica12));
			            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			            pdfTable.addCell(cell1);
			
			            cell1 = new PdfPCell(new Phrase("Typ", helvetica12));
			            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			            pdfTable.addCell(cell1);
			
			            cell1 = new PdfPCell(new Phrase("Kwota", helvetica12));
			            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			            pdfTable.addCell(cell1);
			            
			            cell1 = new PdfPCell(new Phrase("Termin zap�aty", helvetica12));
			            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			            pdfTable.addCell(cell1);
			            
			            pdfTable.setHeaderRows(1);
			            
			            ChargeElectricity chargeElectricity;
			    		ChargeGarbage chargeGarbage;
			    		ChargeGas chargeGas;
			    		ChargeHeating chargeHeating;
			    		ChargeOther chargeOther;
			    		ChargeSevage chargeSevage;
			    		ChargeWater chargeWater;
			    		
			    		Iterator iteratorChargeElectricity = user.getChargeElectricity().iterator();
						Iterator iteratorChargeGarbage = user.getChargeGarbage().iterator();
						Iterator iteratorChargeGas = user.getChargeGas().iterator();
						Iterator iteratorChargeHeating = user.getChargeHeating().iterator();
						Iterator iteratorChargeOther = user.getChargeOther().iterator();
						Iterator iteratorChargeSevage = user.getChargeSevage().iterator();
						Iterator iteratorChargeWater = user.getChargeWater().iterator();
						
						while(iteratorChargeElectricity.hasNext())
						{
							chargeElectricity = ((ChargeElectricity)iteratorChargeElectricity.next());
							if(chargeElectricity.isStatus() == true && chargeElectricity.isReported() == false) {
								pdfTable.addCell(chargeElectricity.getName());
								pdfTable.addCell(new Phrase("Pr�d", helvetica12));
								pdfTable.addCell(String.valueOf(chargeElectricity.getAmountToPay()));
								pdfTable.addCell(String.valueOf(chargeElectricity.getPaymentDate()));
								chargeElectricity.setReported(true);
							}
						}
						while(iteratorChargeGarbage.hasNext())
						{
							chargeGarbage = ((ChargeGarbage)iteratorChargeGarbage.next());
							if(chargeGarbage.isStatus() == true && chargeGarbage.isReported() == false) {
								pdfTable.addCell(chargeGarbage.getName());
								pdfTable.addCell(new Phrase("�mieci", helvetica12));
								pdfTable.addCell(String.valueOf(chargeGarbage.getAmountToPay()));
								pdfTable.addCell(String.valueOf(chargeGarbage.getPaymentDate()));
								chargeGarbage.setReported(true);
							}
						}
						while(iteratorChargeGas.hasNext())
						{
							chargeGas = ((ChargeGas)iteratorChargeGas.next());
							if(chargeGas.isStatus() == true && chargeGas.isReported() == false) {
								pdfTable.addCell(chargeGas.getName());
								pdfTable.addCell("Gaz");
								pdfTable.addCell(String.valueOf(chargeGas.getAmountToPay()));
								pdfTable.addCell(String.valueOf(chargeGas.getPaymentDate()));
								chargeGas.setReported(true);
							}
						}
						while(iteratorChargeHeating.hasNext())
						{
							chargeHeating = ((ChargeHeating)iteratorChargeHeating.next());
							if(chargeHeating.isStatus() == true && chargeHeating.isReported() == false) {
								pdfTable.addCell(chargeHeating.getName());
								pdfTable.addCell("Ogrzewanie");
								pdfTable.addCell(String.valueOf(chargeHeating.getAmountToPay()));
								pdfTable.addCell(String.valueOf(chargeHeating.getPaymentDate()));
								chargeHeating.setReported(true);
							}
						}
						while(iteratorChargeOther.hasNext())
						{
							chargeOther = ((ChargeOther)iteratorChargeOther.next());
							if(chargeOther.isStatus() == true && chargeOther.isReported() == false) {
								pdfTable.addCell(chargeOther.getName());
								pdfTable.addCell("Inne");
								pdfTable.addCell(String.valueOf(chargeOther.getAmountToPay()));
								pdfTable.addCell(String.valueOf(chargeOther.getPaymentDate()));
								chargeOther.setReported(true);
							}
						}
						while(iteratorChargeSevage.hasNext())
						{
							chargeSevage = ((ChargeSevage)iteratorChargeSevage.next());
							if(chargeSevage.isStatus() == true && chargeSevage.isReported() == false) {
								pdfTable.addCell(chargeSevage.getName());
								pdfTable.addCell(new Phrase("�cieki", helvetica12));
								pdfTable.addCell(String.valueOf(chargeSevage.getAmountToPay()));
								pdfTable.addCell(String.valueOf(chargeSevage.getPaymentDate()));
								chargeSevage.setReported(true);
							}
						}
						while(iteratorChargeWater.hasNext())
						{
							chargeWater = ((ChargeWater)iteratorChargeWater.next());
							if(chargeWater.isStatus() == true && chargeWater.isReported() == false) {
								pdfTable.addCell(chargeWater.getName());
								pdfTable.addCell("Woda");
								pdfTable.addCell(String.valueOf(chargeWater.getAmountToPay()));
								pdfTable.addCell(String.valueOf(chargeWater.getPaymentDate()));
								chargeWater.setReported(true);
							}
						}
						
			            document.add(pdfTable);
			            
			            Paragraph paragraph3 = new Paragraph("Dane lokalu:", helvetica12Bold);
			            Paragraph paragraph4 = new Paragraph(user.getStreet() + " " 
			            + user.getBuildingNumber() + ", " + user.getCity(), helvetica12);
			            Paragraph paragraph5 = new Paragraph("Osoba odpowiedzialna: " 
			            + user.getFirstname() + " " + user.getSurname(), helvetica12);
			            
			            document.add(paragraph3);
			            document.add(paragraph4);
			            document.add(paragraph5);
			            
			            document.close();
			            
			            byte[] bytePDF = stream.toByteArray();
			            user.setPdf(bytePDF);
			            userService.editUserWithoutPassword(user);
			
			        } catch (Exception e) {
			            e.printStackTrace();
			        }
				}
			}
		}
	}
	
	public void generateYearlyPDF(Integer year) {
		DateFormat dateFormat = new SimpleDateFormat("YYYY");
		DateFormat dateFormat2 = new SimpleDateFormat("dd.MM.yyyy");
		
		List<User> listOfUsers = userService.getAllUsers();
		
		for(User user: listOfUsers) {
			Set<UserRole> userRoleList = user.getUserRole();
			
			for(UserRole userRole: userRoleList) {
				if(userRole.getRole().contains("ROLE_USER")) {
					
					try {
			            
			            Document document = new Document();
			            
			            ByteArrayOutputStream stream = new ByteArrayOutputStream();
			            PdfWriter.getInstance(document, stream);
			            
			            document.open();

			            Paragraph paragraph = new Paragraph("Pabianice, " 
			            + dateFormat2.format(new Date()).toString() + "r.");
			            
			            paragraph.setAlignment(Element.ALIGN_RIGHT);
			            document.add(paragraph);
			            
			            document.add(new Paragraph(""));

			            document.addTitle("Zestawienie " + user.getBuildingNumber() 
			            + "#" + user.getFlatNumber() + " - " + year);
			
			            //Create Paragraph
			            BaseFont helvetica = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1250, BaseFont.EMBEDDED);
			            Font helvetica16 = new Font(helvetica, 16, Font.BOLD);
			            Font helvetica12Bold = new Font(helvetica, 12, Font.BOLD);
			            Font helvetica12 = new Font(helvetica, 12);
			            
			            Paragraph paragraph2 = new Paragraph("Zestawienie za rok: " + year + ".", helvetica16);
			            paragraph2.add(new Paragraph(" "));
			            paragraph2.setAlignment(Element.ALIGN_CENTER);
			            document.add(paragraph2);
			
			            //Create a table in PDF
			            PdfPTable pdfTable = new PdfPTable(4);
			            PdfPCell cell1 = new PdfPCell(new Phrase("Nazwa op�aty", helvetica12));
			            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			            pdfTable.addCell(cell1);
			
			            cell1 = new PdfPCell(new Phrase("Typ", helvetica12));
			            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			            pdfTable.addCell(cell1);
			
			            cell1 = new PdfPCell(new Phrase("Kwota", helvetica12));
			            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			            pdfTable.addCell(cell1);
			            
			            cell1 = new PdfPCell(new Phrase("Termin zap�aty", helvetica12));
			            cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			            pdfTable.addCell(cell1);
			            
			            pdfTable.setHeaderRows(1);
			            
			            ChargeElectricity chargeElectricity;
			    		ChargeGarbage chargeGarbage;
			    		ChargeGas chargeGas;
			    		ChargeHeating chargeHeating;
			    		ChargeOther chargeOther;
			    		ChargeSevage chargeSevage;
			    		ChargeWater chargeWater;
			    		
			    		Iterator iteratorChargeElectricity = user.getChargeElectricity().iterator();
						Iterator iteratorChargeGarbage = user.getChargeGarbage().iterator();
						Iterator iteratorChargeGas = user.getChargeGas().iterator();
						Iterator iteratorChargeHeating = user.getChargeHeating().iterator();
						Iterator iteratorChargeOther = user.getChargeOther().iterator();
						Iterator iteratorChargeSevage = user.getChargeSevage().iterator();
						Iterator iteratorChargeWater = user.getChargeWater().iterator();
						
						while(iteratorChargeElectricity.hasNext())
						{
							chargeElectricity = ((ChargeElectricity)iteratorChargeElectricity.next());
							
							Date date = chargeElectricity.getPaymentDate();
							String dateString = dateFormat.format(date).toString();	
							
							if(chargeElectricity.isStatus() == true && dateString.contains(year.toString())) {
								pdfTable.addCell(chargeElectricity.getName());
								pdfTable.addCell(new Phrase("Pr�d", helvetica12));
								pdfTable.addCell(String.valueOf(chargeElectricity.getAmountToPay()));
								pdfTable.addCell(String.valueOf(chargeElectricity.getPaymentDate()));
							}
						}
						while(iteratorChargeGarbage.hasNext())
						{
							chargeGarbage = ((ChargeGarbage)iteratorChargeGarbage.next());
							
							Date date = chargeGarbage.getPaymentDate();
							String dateString = dateFormat.format(date).toString();	
							
							if(chargeGarbage.isStatus() == true && dateString.contains(year.toString())) {
								pdfTable.addCell(chargeGarbage.getName());
								pdfTable.addCell(new Phrase("�mieci", helvetica12));
								pdfTable.addCell(String.valueOf(chargeGarbage.getAmountToPay()));
								pdfTable.addCell(String.valueOf(chargeGarbage.getPaymentDate()));
							}
						}
						while(iteratorChargeGas.hasNext())
						{
							chargeGas = ((ChargeGas)iteratorChargeGas.next());
							
							Date date = chargeGas.getPaymentDate();
							String dateString = dateFormat.format(date).toString();	
							
							if(chargeGas.isStatus() == true && dateString.contains(year.toString())) {
								pdfTable.addCell(chargeGas.getName());
								pdfTable.addCell("Gaz");
								pdfTable.addCell(String.valueOf(chargeGas.getAmountToPay()));
								pdfTable.addCell(String.valueOf(chargeGas.getPaymentDate()));
							}
						}
						while(iteratorChargeHeating.hasNext())
						{
							chargeHeating = ((ChargeHeating)iteratorChargeHeating.next());
							
							Date date = chargeHeating.getPaymentDate();
							String dateString = dateFormat.format(date).toString();	
							
							if(chargeHeating.isStatus() == true && dateString.contains(year.toString())) {
								pdfTable.addCell(chargeHeating.getName());
								pdfTable.addCell("Ogrzewanie");
								pdfTable.addCell(String.valueOf(chargeHeating.getAmountToPay()));
								pdfTable.addCell(String.valueOf(chargeHeating.getPaymentDate()));
							}
						}
						while(iteratorChargeOther.hasNext())
						{
							chargeOther = ((ChargeOther)iteratorChargeOther.next());
							
							Date date = chargeOther.getPaymentDate();
							String dateString = dateFormat.format(date).toString();	
							
							if(chargeOther.isStatus() == true && dateString.contains(year.toString())) {
								pdfTable.addCell(chargeOther.getName());
								pdfTable.addCell("Inne");
								pdfTable.addCell(String.valueOf(chargeOther.getAmountToPay()));
								pdfTable.addCell(String.valueOf(chargeOther.getPaymentDate()));
							}
						}
						while(iteratorChargeSevage.hasNext())
						{
							chargeSevage = ((ChargeSevage)iteratorChargeSevage.next());
							
							Date date = chargeSevage.getPaymentDate();
							String dateString = dateFormat.format(date).toString();	
							
							if(chargeSevage.isStatus() == true && dateString.contains(year.toString())) {
								pdfTable.addCell(chargeSevage.getName());
								pdfTable.addCell(new Phrase("�cieki", helvetica12));
								pdfTable.addCell(String.valueOf(chargeSevage.getAmountToPay()));
								pdfTable.addCell(String.valueOf(chargeSevage.getPaymentDate()));
							}
						}
						while(iteratorChargeWater.hasNext())
						{
							chargeWater = ((ChargeWater)iteratorChargeWater.next());
							
							Date date = chargeWater.getPaymentDate();
							String dateString = dateFormat.format(date).toString();	
							
							if(chargeWater.isStatus() == true && dateString.contains(year.toString())) {
								pdfTable.addCell(chargeWater.getName());
								pdfTable.addCell("Woda");
								pdfTable.addCell(String.valueOf(chargeWater.getAmountToPay()));
								pdfTable.addCell(String.valueOf(chargeWater.getPaymentDate()));
							}
						}
						
			            document.add(pdfTable);
			            
			            Paragraph paragraph3 = new Paragraph("Dane lokalu:", helvetica12Bold);
			            Paragraph paragraph4 = new Paragraph(user.getStreet() + " " 
			            + user.getBuildingNumber() + ", " + user.getCity(), helvetica12);
			            Paragraph paragraph5 = new Paragraph("Osoba odpowiedzialna: " 
			            + user.getFirstname() + " " + user.getSurname(), helvetica12);
			            
			            document.add(paragraph3);
			            document.add(paragraph4);
			            document.add(paragraph5);
			            
			            document.close();
			            
			            byte[] bytePDF = stream.toByteArray();
			            user.setPdfYearly(bytePDF);
			            userService.editUserWithoutPassword(user);
			
			        } catch (Exception e) {
			            e.printStackTrace();
			        }
				}
			}
		}
	}
}
