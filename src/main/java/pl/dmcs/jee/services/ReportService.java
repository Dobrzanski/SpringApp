package pl.dmcs.jee.services;

public interface ReportService {
	public void generatePDF();
	public void generateYearlyPDF(Integer year);
}
