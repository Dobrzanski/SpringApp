package pl.dmcs.jee.services;

import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.dmcs.jee.dao.ChargeDao;
import pl.dmcs.jee.dao.UserDao;
import pl.dmcs.jee.domain.ChargeElectricity;
import pl.dmcs.jee.domain.ChargeGarbage;
import pl.dmcs.jee.domain.ChargeGas;
import pl.dmcs.jee.domain.ChargeHeating;
import pl.dmcs.jee.domain.ChargeOther;
import pl.dmcs.jee.domain.ChargeSevage;
import pl.dmcs.jee.domain.ChargeWater;
import pl.dmcs.jee.domain.User;
import pl.dmcs.jee.domain.UserRole;

@Service("chargeService")
@Transactional
public class ChargeServiceImpl implements ChargeService {
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private ChargeDao chargeDao;
	
	@Autowired
	private UserService userService;
	
	@Override
	public void calculateChargeElectricity(ChargeElectricity chargeElectricity) {
		
		List<User> usersList = userService.getAllUsers();
		
		for(User u: usersList) {

			Set<UserRole> userRoleList = u.getUserRole();
			
			for(UserRole uR: userRoleList) {
				if(uR.getRole().contains("ROLE_USER")) {
					chargeElectricity.setInitialState(u.getStateElectricity());
					u.getChargeElectricity().add(chargeElectricity);
					userDao.editUser(u);
				} 
			}
		}
	}

	@Override
	public void calculateChargeGarbage(ChargeGarbage chargeGarbage) {
		
		List<User> usersList = userService.getAllUsers();
		
		for(User u: usersList) {
			
			Set<UserRole> userRoleList = u.getUserRole();
			
			for(UserRole uR: userRoleList) {
				if(uR.getRole().contains("ROLE_USER")) {
					chargeGarbage.setNumberOfPeople(u.getPeopleNumber());
					chargeGarbage.setAmountToPay(chargeGarbage.getNumberOfPeople() * chargeGarbage.getPricePerPerson());
					u.getChargeGarbage().add(chargeGarbage);
					userDao.editUser(u);
				}
			}
		}
	}

	@Override
	public void calculateChargeGas(ChargeGas chargeGas) {

		List<User> usersList = userService.getAllUsers();
		
		for(User u: usersList) {
			
			Set<UserRole> userRoleList = u.getUserRole();
			
			for(UserRole uR: userRoleList) {
				if(uR.getRole().contains("ROLE_USER")) {
					chargeGas.setInitialState(u.getStateGas());
					u.getChargeGas().add(chargeGas);
					userDao.editUser(u);
				}
			}
		}	
	}

	@Override
	public void calculateChargeHeating(ChargeHeating chargeHeating) {
		
		List<User> usersList = userService.getAllUsers();
		
		for(User u: usersList) {
			
			Set<UserRole> userRoleList = u.getUserRole();
			
			for(UserRole uR: userRoleList) {
				if(uR.getRole().contains("ROLE_USER")) {
					chargeHeating.setInitialState(u.getStateHeating());
					u.getChargeHeating().add(chargeHeating);
					userDao.editUser(u);
				}
			}
		}
	}

	@Override
	public void calculateChargeOther(ChargeOther chargeOther) {

		List<User> usersList = userService.getAllUsers();
		
		for(User u: usersList) {
			
			Set<UserRole> userRoleList = u.getUserRole();
			
			for(UserRole uR: userRoleList) {
				if(uR.getRole().contains("ROLE_USER")) {
					u.getChargeOther().add(chargeOther);
					userDao.editUser(u);
				}
			}			
		}
	}

	@Override
	public void calculateChargeSevage(ChargeSevage chargeSevage) {
		
		List<User> usersList = userService.getAllUsers();
		
		for(User u: usersList) {
			
			Set<UserRole> userRoleList = u.getUserRole();
			
			for(UserRole uR: userRoleList) {
				if(uR.getRole().contains("ROLE_USER")) {
					chargeSevage.setInitialState(u.getStateSevage());
					u.getChargeSevage().add(chargeSevage);
					userDao.editUser(u);
				}
			}
		}
	}

	@Override
	public void calculateChargeWater(ChargeWater chargeWater) {

		List<User> usersList = userService.getAllUsers();
		
		for(User u: usersList) {
			
			Set<UserRole> userRoleList = u.getUserRole();
			
			for(UserRole uR: userRoleList) {
				if(uR.getRole().contains("ROLE_USER")) {
					chargeWater.setInitialStateCold(u.getStateColdWater());
					chargeWater.setInitialStateHot(u.getStateHotWater());
					u.getChargeWater().add(chargeWater);
					userDao.editUser(u);
				}
			}
		}
	}

	@Override
	public ChargeElectricity findChargeElectricityById(Integer chargeId) {
		return chargeDao.findChargeElectricityById(chargeId);
	}

	@Override
	public ChargeGarbage findChargeGarbageById(Integer chargeId) {
		return chargeDao.findChargeGarbageById(chargeId);
	}

	@Override
	public ChargeGas findChargeGasById(Integer chargeId) {
		return chargeDao.findChargeGasById(chargeId);
	}

	@Override
	public ChargeHeating findChargeHeatingById(Integer chargeId) {
		return chargeDao.findChargeHeatingById(chargeId);
	}

	@Override
	public ChargeOther findChargeOtherById(Integer chargeId) {
		return chargeDao.findChargeOtherById(chargeId);
	}

	@Override
	public ChargeSevage findChargeSevageById(Integer chargeId) {
		return chargeDao.findChargeSevageById(chargeId);
	}

	@Override
	public ChargeWater findChargeWaterById(Integer chargeId) {
		return chargeDao.findChargeWaterById(chargeId);
	}

	@Override
	public void payForChargeElectricity(ChargeElectricity chargeElectricity) {
		chargeDao.payForChargeElectricity(chargeElectricity);
	}

	@Override
	public void payForChargeGarbage(ChargeGarbage chargeGarbage) {
		chargeDao.payForChargeGarbage(chargeGarbage);
	}

	@Override
	public void payForChargeGas(ChargeGas chargeGas) {
		chargeDao.payForChargeGas(chargeGas);
	}

	@Override
	public void payForChargeHeating(ChargeHeating chargeHeating) {
		chargeDao.payForChargeHeating(chargeHeating);
	}

	@Override
	public void payForChargeOther(ChargeOther chargeOther) {
		chargeDao.payForChargeOther(chargeOther);
	}

	@Override
	public void payForChargeSevage(ChargeSevage chargeSevage) {
		chargeDao.payForChargeSevage(chargeSevage);
		
	}

	@Override
	public void payForChargeWater(ChargeWater chargeWater) {
		chargeDao.payForChargeWater(chargeWater);
	}
}
