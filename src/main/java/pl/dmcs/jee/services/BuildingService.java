package pl.dmcs.jee.services;

import java.util.List;

import pl.dmcs.jee.domain.Building;

public interface BuildingService {
	List<Building> getAllBuildings();
	Building findBuilding(Integer buildingId);
	Building findBuilding(String city, String street, Integer number);
	void addBuilding(Building building);
	void updateBuilding(Building building);
	void removeBuilding(Integer buildingId);
	boolean isBuildingUnique(String city, String street, Integer number);
}
