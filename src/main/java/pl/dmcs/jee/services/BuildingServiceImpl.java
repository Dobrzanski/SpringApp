package pl.dmcs.jee.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.dmcs.jee.dao.BuildingDao;
import pl.dmcs.jee.domain.Building;

@Service("buildingService")
@Transactional
public class BuildingServiceImpl implements BuildingService {

	@Autowired
	private BuildingDao buildingDao;
	
	@Override
	public List<Building> getAllBuildings() {
		return buildingDao.getAllBuildings();
	}

	@Override
	public Building findBuilding(Integer buildingId) {
		return buildingDao.findBuilding(buildingId);
	}

	@Override
	public Building findBuilding(String city, String street, Integer number) {
		return buildingDao.findBuilding(city, street, number);
	}

	@Override
	public void addBuilding(Building building) {
		buildingDao.addBuilding(building);
	}

	@Override
	public void updateBuilding(Building building) {
		Building entity = buildingDao.findBuilding(building.getBuildingId());
		if(entity != null) { 
			entity.setCity(building.getCity());
			entity.setStreet(building.getStreet());
			entity.setNumber(building.getNumber());
		}
	}

	@Override
	public void removeBuilding(Integer buildingId) {
		buildingDao.removeBuilding(buildingId);
	}

	@Override
	public boolean isBuildingUnique(String city, String street, Integer number) {
		Building building = findBuilding(city, street, number);
		if(building == null) {
			return true;
		} else {
			return false;
		}
	}

}
