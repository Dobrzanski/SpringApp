package pl.dmcs.jee.services;

import pl.dmcs.jee.domain.ChargeElectricity;
import pl.dmcs.jee.domain.ChargeGarbage;
import pl.dmcs.jee.domain.ChargeGas;
import pl.dmcs.jee.domain.ChargeHeating;
import pl.dmcs.jee.domain.ChargeOther;
import pl.dmcs.jee.domain.ChargeSevage;
import pl.dmcs.jee.domain.ChargeWater;

public interface ChargeService {
	public void calculateChargeElectricity(ChargeElectricity chargeElectricity);
	public void calculateChargeGarbage(ChargeGarbage chargeGarbage);
	public void calculateChargeGas(ChargeGas chargeGas);
	public void calculateChargeHeating(ChargeHeating chargeHeating);
	public void calculateChargeOther(ChargeOther chargeOther);
	public void calculateChargeSevage(ChargeSevage chargeSevage);
	public void calculateChargeWater(ChargeWater chargeWater);
	
	public ChargeElectricity findChargeElectricityById(Integer chargeId);
	public ChargeGarbage findChargeGarbageById(Integer chargeId);
	public ChargeGas findChargeGasById(Integer chargeId);
	public ChargeHeating findChargeHeatingById(Integer chargeId);
	public ChargeOther findChargeOtherById(Integer chargeId);
	public ChargeSevage findChargeSevageById(Integer chargeId);
	public ChargeWater findChargeWaterById(Integer chargeId);
	
	public void payForChargeElectricity(ChargeElectricity chargeElectricity);
	public void payForChargeGarbage(ChargeGarbage chargeGarbage);
	public void payForChargeGas(ChargeGas chargeGas);
	public void payForChargeHeating(ChargeHeating chargeHeating);
	public void payForChargeOther(ChargeOther chargeOther);
	public void payForChargeSevage(ChargeSevage chargeSevage);
	public void payForChargeWater(ChargeWater chargeWater);
}
