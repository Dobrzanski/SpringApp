package pl.dmcs.jee.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pl.dmcs.jee.dao.UserDao;
import pl.dmcs.jee.domain.User;
import pl.dmcs.jee.domain.UserRole;

import javax.transaction.Transactional;
import java.util.List;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	@Override
	public List<User> getAllUsers() {
		return userDao.getAllUsers();
	}

	@Override
	public User findUserById(Integer userId) {
		return userDao.findUserById(userId);
	}

	@Override
	public User findUserByMail(String mail) {
		return userDao.findUserByMail(mail);
	}

	@Override
	public void addUser(User user) {
		user.getUserRole().add(userDao.findRoleByName("ROLE_USER"));
		user.setPassword(hashPassword(user.getPassword()));
		userDao.addUser(user);
	}

	@Override
	public void editUser(User user) {
		user.getUserRole().add(userDao.findRoleByName("ROLE_USER"));
		user.setPassword(hashPassword(user.getPassword()));
		user.setUserId(user.getUserId());
		user.setPdf(user.getPdf());
		user.setPdf(user.getPdf());
		userDao.editUser(user);
	}
	
	@Override
	public void editUserWithoutPassword(User user) {
		user.setUserId(user.getUserId());
		userDao.editUser(user);
	}

	@Override
	public void removeUser(Integer userId) {
		userDao.removeUser(userId);
	}

	@Override
	public boolean isUserUnique(String mail) {
		User user = findUserByMail(mail);
		if (user == null) {
			return true;
		} else {
			return false;
		}
	}
	
	public String hashPassword(String password) {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		return passwordEncoder.encode(password);
	}

	@Override
	public String findMailById(Integer userId) {
		User user = findUserById(userId);
		String mail = user.getMail();
		
		return mail;
	}
	
	@Override
	public UserRole getUserRole(int id) {
		return userDao.getUserRole(id);
	}


}
