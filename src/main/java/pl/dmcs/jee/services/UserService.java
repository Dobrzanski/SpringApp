package pl.dmcs.jee.services;

import java.util.List;

import pl.dmcs.jee.domain.User;
import pl.dmcs.jee.domain.UserRole;

public interface UserService {

	public List<User> getAllUsers();
	public User findUserById(Integer userId);
	public User findUserByMail(String mail);
	public void addUser(User user);
	public void editUser(User user);
	public void editUserWithoutPassword(User user);
	public void removeUser(Integer userId);
	public boolean isUserUnique(String mail);
	public String hashPassword(String password);
	public String findMailById(Integer userId);
	public UserRole getUserRole(int id);
}
