package pl.dmcs.jee.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table
public class User {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column
	private Integer userId;
	
	@Column 
	private String city;
	
	@Column
	private String street;
	
	@Column
	private Integer buildingNumber;
	
	@Column
	private Integer flatNumber;
	
	@Column
	private Integer peopleNumber;
	
	@Column
	private String firstname;
	
	@Column
	private String surname;
	
	@Column
	private String mail;
	
	@Column
	private String password;
	
	@Column 
	private String phone;
	
	@Lob
	private byte[] pdf;
	
	@Lob
	private byte[] pdfYearly;
	
	@Column
	private double stateColdWater;
	
	@Column
	private double stateHotWater;
	
	@Column
	private double stateElectricity;
	
	@Column
	private double stateGas;
	
	@Column
	private double stateHeating;
	
	@Column
	private double stateSevage;

	@Column
	private boolean enabled;
	
	@ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	private Set<UserRole> userRole = new HashSet<UserRole>(0);
	
	@ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	private Set<ChargeElectricity> chargeElectricity = new HashSet<ChargeElectricity>(0);
	
	@ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	private Set<ChargeGarbage> chargeGarbage = new HashSet<ChargeGarbage>(0);
	
	@ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	private Set<ChargeGas> chargeGas = new HashSet<ChargeGas>(0);
	
	@ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	private Set<ChargeHeating> chargeHeating = new HashSet<ChargeHeating>(0);
	
	@ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	private Set<ChargeOther> chargeOther = new HashSet<ChargeOther>(0);
	
	@ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	private Set<ChargeSevage> chargeSevage = new HashSet<ChargeSevage>(0);
	
	@ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	private Set<ChargeWater> chargeWater = new HashSet<ChargeWater>(0);
	
	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public Integer getBuildingNumber() {
		return buildingNumber;
	}

	public void setBuildingNumber(Integer buildingNumber) {
		this.buildingNumber = buildingNumber;
	}

	public Integer getFlatNumber() {
		return flatNumber;
	}

	public void setFlatNumber(Integer flatNumber) {
		this.flatNumber = flatNumber;
	}

	public Integer getPeopleNumber() {
		return peopleNumber;
	}

	public void setPeopleNumber(Integer peopleNumber) {
		this.peopleNumber = peopleNumber;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public byte[] getPdf() {
		return pdf;
	}

	public void setPdf(byte[] pdf) {
		this.pdf = pdf;
	}
	
	public byte[] getPdfYearly() {
		return pdfYearly;
	}

	public void setPdfYearly(byte[] pdfYearly) {
		this.pdfYearly = pdfYearly;
	}

	public double getStateColdWater() {
		return stateColdWater;
	}

	public void setStateColdWater(double stateColdWater) {
		this.stateColdWater = stateColdWater;
	}

	public double getStateHotWater() {
		return stateHotWater;
	}

	public void setStateHotWater(double stateHotWater) {
		this.stateHotWater = stateHotWater;
	}

	public double getStateElectricity() {
		return stateElectricity;
	}

	public void setStateElectricity(double stateElectricity) {
		this.stateElectricity = stateElectricity;
	}

	public double getStateGas() {
		return stateGas;
	}

	public void setStateGas(double stateGas) {
		this.stateGas = stateGas;
	}

	public double getStateHeating() {
		return stateHeating;
	}

	public void setStateHeating(double stateHeating) {
		this.stateHeating = stateHeating;
	}

	public double getStateSevage() {
		return stateSevage;
	}

	public void setStateSevage(double stateSevage) {
		this.stateSevage = stateSevage;
	}

	public boolean isEnabled() {
		return enabled;
	}
	
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public Set<UserRole> getUserRole() {
		return userRole;
	}

	public void setUserRole(Set<UserRole> userRole) {
		this.userRole = userRole;
	}

	public Set<ChargeElectricity> getChargeElectricity() {
		return chargeElectricity;
	}

	public void setChargeElectricity(Set<ChargeElectricity> chargeElectricity) {
		this.chargeElectricity = chargeElectricity;
	}

	public Set<ChargeGarbage> getChargeGarbage() {
		return chargeGarbage;
	}

	public void setChargeGarbage(Set<ChargeGarbage> chargeGarbage) {
		this.chargeGarbage = chargeGarbage;
	}

	public Set<ChargeGas> getChargeGas() {
		return chargeGas;
	}

	public void setChargeGas(Set<ChargeGas> chargeGas) {
		this.chargeGas = chargeGas;
	}

	public Set<ChargeHeating> getChargeHeating() {
		return chargeHeating;
	}

	public void setChargeHeating(Set<ChargeHeating> chargeHeating) {
		this.chargeHeating = chargeHeating;
	}

	public Set<ChargeOther> getChargeOther() {
		return chargeOther;
	}

	public void setChargeOther(Set<ChargeOther> chargeOther) {
		this.chargeOther = chargeOther;
	}

	public Set<ChargeSevage> getChargeSevage() {
		return chargeSevage;
	}

	public void setChargeSevage(Set<ChargeSevage> chargeSevage) {
		this.chargeSevage = chargeSevage;
	}

	public Set<ChargeWater> getChargeWater() {
		return chargeWater;
	}

	public void setChargeWater(Set<ChargeWater> chargeWater) {
		this.chargeWater = chargeWater;
	}
}
