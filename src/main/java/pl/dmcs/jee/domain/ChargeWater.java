package pl.dmcs.jee.domain;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class ChargeWater {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column
	private Integer chargeId;
	
	@Column
	private String name;
	
	@Column
	private double initialStateCold;
	
	@Column
	private double finalStateCold;
	
	@Column
	private double initialStateHot;
	
	@Column
	private double finalStateHot;
	
	@Column
	private double pricePerUnitCold;
	
	@Column
	private double pricePerUnitHot;
	
	@Column
	private Date paymentDate;
	
	@Column
	private double lumpSumCold;
	
	@Column
	private double lumpSumHot;
	
	@Column
	private double amountToPay;
	
	@Column
	private boolean reported;
	
	@Column
	private boolean status; //czy zap�acone
	
	private String captchaCode;

	public Integer getChargeId() {
		return chargeId;
	}

	public void setChargeId(Integer chargeId) {
		this.chargeId = chargeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getInitialStateCold() {
		return initialStateCold;
	}

	public void setInitialStateCold(double initialStateCold) {
		this.initialStateCold = initialStateCold;
	}

	public double getFinalStateCold() {
		return finalStateCold;
	}

	public void setFinalStateCold(double finalStateCold) {
		this.finalStateCold = finalStateCold;
	}

	public double getInitialStateHot() {
		return initialStateHot;
	}

	public void setInitialStateHot(double initialStateHot) {
		this.initialStateHot = initialStateHot;
	}

	public double getFinalStateHot() {
		return finalStateHot;
	}

	public void setFinalStateHot(double finalStateHot) {
		this.finalStateHot = finalStateHot;
	}

	public double getPricePerUnitCold() {
		return pricePerUnitCold;
	}

	public void setPricePerUnitCold(double pricePerUnitCold) {
		this.pricePerUnitCold = pricePerUnitCold;
	}

	public double getPricePerUnitHot() {
		return pricePerUnitHot;
	}

	public void setPricePerUnitHot(double pricePerUnitHot) {
		this.pricePerUnitHot = pricePerUnitHot;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public double getLumpSumCold() {
		return lumpSumCold;
	}

	public void setLumpSumCold(double lumpSumCold) {
		this.lumpSumCold = lumpSumCold;
	}
	
	public double getLumpSumHot() {
		return lumpSumHot;
	}

	public void setLumpSumHot(double lumpSumHot) {
		this.lumpSumHot = lumpSumHot;
	}
	
	public double getAmountToPay() {
		return amountToPay;
	}

	public void setAmountToPay(double amountToPay) {
		this.amountToPay = amountToPay;
	}

	public boolean isReported() {
		return reported;
	}

	public void setReported(boolean reported) {
		this.reported = reported;
	}
	
	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
	public String getCaptchaCode() {
		return captchaCode;
	}

	public void setCaptchaCode(String userCaptchaCode) {
		this.captchaCode = userCaptchaCode;
	}
}
