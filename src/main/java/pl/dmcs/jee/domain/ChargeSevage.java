package pl.dmcs.jee.domain;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class ChargeSevage {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column
	private Integer chargeId;
	
	@Column
	private String name;
	
	@Column
	private double initialState;
	
	@Column
	private double finalState;
	
	@Column
	private double pricePerUnit;
	
	@Column
	private Date paymentDate;
	
	@Column
	private double lumpSum;
	
	@Column
	private double amountToPay;
	
	@Column
	private boolean reported;
	
	@Column
	private boolean status; //czy zap�acone
	
	private String captchaCode;

	public Integer getChargeId() {
		return chargeId;
	}

	public void setChargeId(Integer chargeId) {
		this.chargeId = chargeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getInitialState() {
		return initialState;
	}

	public void setInitialState(double initialState) {
		this.initialState = initialState;
	}

	public double getFinalState() {
		return finalState;
	}

	public void setFinalState(double finalState) {
		this.finalState = finalState;
	}

	public double getPricePerUnit() {
		return pricePerUnit;
	}

	public void setPricePerUnit(double pricePerUnit) {
		this.pricePerUnit = pricePerUnit;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public double getLumpSum() {
		return lumpSum;
	}

	public void setLumpSum(double lumpSum) {
		this.lumpSum = lumpSum;
	}
	
	public double getAmountToPay() {
		return amountToPay;
	}

	public void setAmountToPay(double amountToPay) {
		this.amountToPay = amountToPay;
	}
	
	public boolean isReported() {
		return reported;
	}

	public void setReported(boolean reported) {
		this.reported = reported;
	}
	
	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
	public String getCaptchaCode() {
		return captchaCode;
	}

	public void setCaptchaCode(String userCaptchaCode) {
		this.captchaCode = userCaptchaCode;
	}
}
