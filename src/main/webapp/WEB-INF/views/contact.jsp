<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<h2>Dane kontaktowe</h2>
<div class="col-md-4 col-md-offset-1">
	<h3>Administracja:</h3>
	<p>ul.Zamkowa 52</p>
	<p>95-200</p>
	<p>Adres e-mail: spoldzieniajee@gmail.com</p>
	<p>tel: 42 215 35 54</p>
</div>
<div class="col-md-4">
	<h3>Mapa dojazdu:</h3>
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2474.729936250355!2d19.342189951374714!3d51.66478080690496!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x471a3639c1b4b8cf%3A0x991bdf493f6d4410!2sZamkowa+52%2C+Pabianice!5e0!3m2!1spl!2spl!4v1461596192194" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>