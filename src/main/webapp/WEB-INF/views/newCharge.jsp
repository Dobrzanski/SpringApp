<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div class="col-lg-2" style="border: 2px solid black; border-radius: 8px; margin-right:30px; margin-top: 10px">
	<a href="<c:url value="/admin/charge/add/electricity"/>" style="text-decoration:none"><i class="fa fa-bolt fa-stack-1x fa-5x"></i>
    <br/><br/><br/><br/>
    <p class="main_p">Prąd</p></a>
</div>
<div class="col-lg-2" style="border: 2px solid black; border-radius: 8px; margin-right:30px; margin-top: 10px">
	<a href="<c:url value="/admin/charge/add/garbage"/>" style="text-decoration:none"><i class="fa fa-trash fa-stack-1x fa-5x"></i>
    <br/><br/><br/><br/>
    <p class="main_p">Odpady</p></a>
</div>
<div class="col-lg-2" style="border: 2px solid black; border-radius: 8px; margin-right:30px; margin-top: 10px">
	<a href="<c:url value="/admin/charge/add/gas"/>" style="text-decoration:none"><i class="fa fa-fire fa-stack-1x fa-5x"></i>
    <br/><br/><br/><br/>
    <p class="main_p">Gaz</p></a>
</div>
<div class="col-lg-2" style="border: 2px solid black; border-radius: 8px; margin-right:30px; margin-top: 10px">
	<a href="<c:url value="/admin/charge/add/heating"/>" style="text-decoration:none"><i class="fa fa-industry fa-stack-1x fa-5x"></i>
    <br/><br/><br/><br/>
    <p class="main_p">Ogrzewanie</p></a>
</div>
<div class="col-lg-2" style="border: 2px solid black; border-radius: 8px; margin-right:30px; margin-top: 10px">
	<a href="<c:url value="/admin/charge/add/sevage"/>" style="text-decoration:none"><i class="fa fa-recycle fa-stack-1x fa-5x"></i>
    <br/><br/><br/><br/>
    <p class="main_p">Ścieki</p></a>
</div>
<div class="col-lg-2" style="border: 2px solid black; border-radius: 8px; margin-right:30px; margin-top: 10px">
	<a href="<c:url value="/admin/charge/add/water"/>" style="text-decoration:none"><i class="fa fa-anchor fa-stack-1x fa-5x"></i>
    <br/><br/><br/><br/>
    <p class="main_p">Woda</p></a>
</div>
<div class="col-lg-2" style="border: 2px solid black; border-radius: 8px; margin-right:30px; margin-top: 10px">
	<a href="<c:url value="/admin/charge/add/other"/>" style="text-decoration:none"><i class="fa fa-home fa-stack-1x fa-5x"></i>
    <br/><br/><br/><br/>
    <p class="main_p">Inne</p></a>
</div>