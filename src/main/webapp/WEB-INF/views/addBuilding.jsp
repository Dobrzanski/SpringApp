<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div class="col-md-4 col-md-offset-4">
	<c:choose>
 		<c:when test="${edit}">
  			<h2>Aktualizacja danych budynku</h2>
  		</c:when>
  		<c:otherwise>
			<h2>Dodawanie budynku</h2>
		</c:otherwise>
	</c:choose>
	<c:choose>
 		<c:when test="${flag}">
			<div class="alert alert-danger" role="alert">
		    	<strong>Taki budynek już istnieje!</strong>
			</div>
		</c:when>
	</c:choose>		
	<form:form method="POST" modelAttribute="building">
			<div class="form-group">
		    	<label for="city">Miasto:</label>
		    	<form:input path="city" id="city" cssClass="form-control"/>
				<form:errors path="city" cssClass="error"/>
		  	</div>
			<div class="form-group">
		    	<label for="street">Nazwa ulicy:</label>
		    	<form:input path="street" id="street" cssClass="form-control"/>
				<form:errors path="street" cssClass="error"/>
		  	</div>
  			<div class="form-group">
    			<label for="number">Numer budynku:</label>
    			<form:input path="number" id="number" cssClass="form-control"/>
				<form:errors path="number" cssClass="error"/>
  			</div>
  			<c:choose>
				<c:when test="${edit}">
					<button type="submit" class="btn btn-default" style="width: 100%;">Aktualizuj dane</button>
  				</c:when>
  				<c:otherwise>
					<button type="submit" class="btn btn-default" style="width: 100%;">Dodaj budynek</button>
				</c:otherwise>
			</c:choose>		
	</form:form>
</div>