<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="botDetect" uri="botDetect"%>


<div class="col-md-4 col-md-offset-4">
	
	<c:choose>
		<c:when test="${user}">
			<h2>Rachunek za ścieki</h2>
		</c:when>
		<c:otherwise>
			<h2>Naliczanie opłaty za ścieki</h2>
		</c:otherwise>
	</c:choose>		
	<c:if test="${user}">
		<script>
			window.onload = function(){
				document.getElementById("name").readOnly = true;
				document.getElementById("paymentDate").readOnly = true;
				document.getElementById("initialState").readOnly = true;
				document.getElementById("amountToPay").readOnly = true;
				document.getElementById("lumpSum").readOnly = true;
				document.getElementById("priceDiv").style.display = 'none';
			};
		</script>
	</c:if>
	<form:form method="POST" modelAttribute="chargeSevage">
		<div class="form-group">
	    	<label for="name">Nazwa opłaty:</label>
	    	<c:choose>
				<c:when test="${user}">
					<form:input path="name" id="name" cssClass="form-control"/>
				</c:when>
 				<c:otherwise>
 					<form:input path="name" id="name" value="Opłata za ścieki | [miesiąc]" cssClass="form-control"/>
 				</c:otherwise>
			</c:choose>
			<form:errors path="name" cssClass="error"/>
	  	</div>
		<div class="form-group">
	    	<label for="paymentDate">Data płatności:</label>
	    	<form:input path="paymentDate" id="paymentDate" cssClass="form-control"/>
			<form:errors path="paymentDate" cssClass="error"/>
	  	</div>
	  	<div class="form-group">
	    	<label for="lumpSum">Ryczałt:</label>
	    	<form:input path="lumpSum" id="lumpSum" cssClass="form-control"/>
			<form:errors path="lumpSum" cssClass="error"/>
	  	</div>
		<c:choose>
			<c:when test="${user}">
				<div class="form-group">
	    			<label for="initialState">Poprzedni stan licznika:</label>
	    			<form:input path="initialState" id="initialState" cssClass="form-control"/>
					<form:errors path="initialState" cssClass="error"/>
	  			</div>
	  			<c:choose>
	  				<c:when test="${lumpSum}">
	  					<div class="form-group">
			    			<label for="finalState">Obecny stan licznika:</label>
			    			<form:input path="finalState" id="finalState" cssClass="form-control"/>
							<form:errors path="finalState" cssClass="error"/>
			  			</div>
	  				</c:when>
	  				<c:otherwise>
	  					<div class="form-group">
			    			<label for="finalState">Obecny stan licznika:</label>
			    			<form:input onkeyup="findTotal()" path="finalState" id="finalState" cssClass="form-control"/>
							<form:errors path="finalState" cssClass="error"/>
			  			</div>
	  				</c:otherwise>
	  			</c:choose>
	  			<div class="form-group">
	    			<label for="amountToPay">Kwota do zapłaty:</label>
	    			<form:input path="amountToPay" id="amountToPay" cssClass="form-control"/>
					<form:errors path="amountToPay" cssClass="error"/>
	  			</div>
	  			<div class="form-group" id="priceDiv">
	    			<label for="pricePerUnit">Cena za jednostkę:</label>
	    			<form:input path="pricePerUnit" id="pricePerUnit" cssClass="form-control"/>
					<form:errors path="pricePerUnit" cssClass="error"/>
	  			</div>
	  			<div class="form-group">
	  				<botDetect:captcha id="exampleCaptcha"/>
	  				<input class="form-control" id="captchaCode" type="text" name="captchaCode" value="${basicExample.captchaCode}"/>
	  				<span class="error">${error}</span>
					<form:errors path="captchaCode" cssClass="error"/>
	  			</div>
			</c:when>
			<c:otherwise>
				<div class="form-group">
	    			<label for="pricePerUnit">Cena za jednostkę:</label>
	    			<form:input path="pricePerUnit" id="pricePerUnit" cssClass="form-control"/>
					<form:errors path="pricePerUnit" cssClass="error"/>
	  			</div>
			</c:otherwise>
		</c:choose>
 			
 		<script type="text/javascript">
			function findTotal(){
			    var initialState = document.getElementById('initialState').value;
			    var finalState = document.getElementById('finalState').value;
			    var pricePerUnit = document.getElementById('pricePerUnit').value;
			    var amountToPay = 0.0;
			    
			    amountToPay = parseFloat(pricePerUnit * (finalState - initialState)).toFixed(2);
  
			    document.getElementById('amountToPay').value = amountToPay;
			}
	    </script>
	    <script type="text/javascript">
		    function isEmpty() {
				if(document.getElementById('finalState').value == "") {
					document.getElementById('finalState').value = "0.0"
				}
			}
	    </script>
		<c:if test="${lumpSum}">
			<script type="text/javascript">
				document.getElementById("finalState").readOnly = true;
			    var initialState = document.getElementById('initialState').value;
			    var lumpSum = document.getElementById('lumpSum').value;
			    var finalState = parseFloat(initialState) + parseFloat(lumpSum);
			    var pricePerUnit = document.getElementById('pricePerUnit').value;
			    var amountToPay = 0.0;
			    
			    amountToPay = parseFloat(pricePerUnit * (finalState - initialState)).toFixed(2);

			    document.getElementById('finalState').value = finalState;
			    document.getElementById('amountToPay').value = amountToPay;
			</script>
		</c:if> 
		<c:choose>
			<c:when test="${user}">
				<button type="submit" onclick="isEmpty()" class="btn btn-default" style="width: 100%;">Opłać rachunek</button>
			</c:when>
			<c:otherwise>
				<button type="submit" onclick="isEmpty()" class="btn btn-default" style="width: 100%;">Nalicz opłatę</button>
			</c:otherwise>
		</c:choose>
 			
 			<script>
			$(function() {
		    	$( "#paymentDate" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();		    
		  	});
		</script>	
	</form:form>
</div>