<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<sec:authentication property="principal.authorities" var="authorities" />
<c:forEach items="${authorities}" var="authority" varStatus="vs">

<div class="container">
	<div id="sidebar-wrapper">
        <ul class="sidebar-nav nav-pills nav-stacked" id="menu">
        		<c:if test="${authority.authority == 'ROLE_ADMIN'}">
	                <li>
	                    <a href="#"><span class="fa-stack fa-lg pull-left"><i class="fa fa-building fa-stack-1x "></i></span>Budynki</a>
	                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
	                    	<li>
	                            <a href="<c:url value="/admin/building/add"/>">
	                                <span class="fa-stack fa-lg pull-left">
	                                    <i class="fa fa-plus-circle fa-stack-1x "></i></span>Dodaj budynek</a>
	                        </li>
	                        <li>
	                            <a href="<c:url value="/admin/building/list"/>">
	                                <span class="fa-stack fa-lg pull-left">
	                                    <i class="fa fa-list fa-stack-1x "></i></span>Wszystkie budynki</a>
	                        </li>
	                    </ul>
	                </li>
	                <li>
	                    <a href="14"><span class="fa-stack fa-lg pull-left"><i class="fa fa-male fa-stack-1x"></i></span>Użytkownicy</a>
	                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
	                        <li>
	                            <a href="<c:url value="/admin/user/add"/>">
	                                <span class="fa-stack fa-lg pull-left">
	                                    <i class="fa fa-user-plus fa-stack-1x "></i></span>Nowy użytkownik</a>
	                        </li>
	                        <li>
	                            <a href="<c:url value="/admin/user/list"/>">
	                            	<span class="fa-stack fa-lg pull-left">
	                            		<i class="fa fa-users fa-stack-1x "></i></span>Wszyscy użytkownicy</a>
	                        </li>
	                    </ul>
	                </li>
	                <li>
	                    <a href="14"><span class="fa-stack fa-lg pull-left"><i class="fa fa-usd fa-stack-1x"></i></span>Opłaty</a>
	                    <ul class="nav-pills nav-stacked" style="list-style-type:none;">
	                        <li>
	                            <a href="<c:url value="/admin/charge/new"/>">
	                            	<span class="fa-stack fa-lg pull-left">
	                            		<i class="fa fa-calculator fa-stack-1x "></i></span>Nalicz opłaty</a>
	                        </li>
	                        <li>
	                            <a href="<c:url value="/admin/charge/list"/>">
	                                <span class="fa-stack fa-lg pull-left">
	                                    <i class="fa fa-area-chart fa-stack-1x"></i></span>Status opłat</a>
	                        </li>
	                        <li>
	                            <a href="<c:url value="/admin/charge/yearlyReport"/>">
	                                <span class="fa-stack fa-lg pull-left">
	                                    <i class="fa fa-list fa-stack-1x"></i></span>Zestawienie roczne</a>
	                        </li>
	                    </ul>
	                </li>
                </c:if>
                <c:if test="${authority.authority == 'ROLE_USER'}">
                	<li>
	                    <a href="<c:url value="/user/edit/${loginId}"/>">
	                    	<span class="fa-stack fa-lg pull-left">
	                    		<i class="fa fa-male fa-stack-1x"></i></span>Ustawienia konta</a>
	                </li>
	                <li>
	                    <a href="<c:url value="/user/charge/list/${loginId}"/>">
	                    	<span class="fa-stack fa-lg pull-left">
	                    		<i class="fa fa-usd fa-stack-1x"></i></span>Rachunki</a>
	                </li>
                </c:if>
               
	            <c:url value="/j_spring_security_logout" var="logoutUrl" />
	 
				<!-- csrt for log out-->
				<form action="${logoutUrl}" method="post" id="logoutForm">
					<input type="hidden" 
						name="${_csrf.parameterName}"
					value="${_csrf.token}" />
				</form>
				<script>
					function formSubmit() {
						document.getElementById("logoutForm").submit();
					}
				</script>
					
	            <li>
					<a href="<c:url value="/logout"/>">
	                    <span class="fa-stack fa-lg pull-left"><i class="fa fa-sign-out fa-stack-1x "></i></span>Wyloguj</a>
	            </li>
        </ul>
    </div>
</div>
</c:forEach>