<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<h2>Strona startowa</h2>
<c:choose>
 		<c:when test="${flag}">
			<div class="alert alert-success" role="alert">
		    	<strong>${success}</strong>
			</div>
		</c:when>
	</c:choose>
<p>${admin}</p>