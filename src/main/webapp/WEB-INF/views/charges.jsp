<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="row">
	<h2>Lista rachunków</h2>
	<table class="table table-bordered">
    	<thead>
      		<tr>
        		<th>Nazwa opłaty</th>
        		<th>Termin płatności</th>
        		<th>Status</th>
        		<th>Wpłata</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${listChargeElectricity}" var="item">
				<tr>
					<td>${item.name}</td>
        			<td>${item.paymentDate}</td>
        			<c:if test="${item.status == false}">
        				<td>Nie zapłacono</td>
        				<td><a class="btnActive" href="<c:url value="/user/charge/pay/electricity/${item.chargeId}"/>">Zapłać</a></td>
        			</c:if>
        			<c:if test="${item.status == true}">
        				<td>Zapłacono</td>
        				<td><a class="btnNotActive" href="<c:url value="/user/charge/pay/electricity/${item.chargeId}"/>">Zapłać</a></td>
        			</c:if>
				</tr>
			</c:forEach>
			<c:forEach items="${listChargeGarbage}" var="item">
				<tr>
					<td>${item.name}</td>
        			<td>${item.paymentDate}</td>
        			<c:if test="${item.status == false}">
        				<td>Nie zapłacono</td>
        				<td><a class="btnActive" href="<c:url value="/user/charge/pay/garbage/${item.chargeId}"/>">Zapłać</a></td>
        			</c:if>
        			<c:if test="${item.status == true}">
        				<td>Zapłacono</td>
        				<td><a class="btnNotActive" href="<c:url value="/user/charge/pay/garbage/${item.chargeId}"/>">Zapłać</a></td>
        			</c:if>
				</tr>
			</c:forEach>
			<c:forEach items="${listChargeGas}" var="item">
				<tr>
					<td>${item.name}</td>
        			<td>${item.paymentDate}</td>
        			<c:if test="${item.status == false}">
        				<td>Nie zapłacono</td>
        				<td><a class="btnActive" href="<c:url value="/user/charge/pay/gas/${item.chargeId}"/>">Zapłać</a></td>
        			</c:if>
        			<c:if test="${item.status == true}">
        				<td>Zapłacono</td>
        				<td><a class="btnNotActive" href="<c:url value="/user/charge/pay/gas/${item.chargeId}"/>">Zapłać</a></td>
        			</c:if>
				</tr>
			</c:forEach>
			<c:forEach items="${listChargeHeating}" var="item">
				<tr>
					<td>${item.name}</td>
        			<td>${item.paymentDate}</td>
        			<c:if test="${item.status == false}">
        				<td>Nie zapłacono</td>
        				<td><a class="btnActive" href="<c:url value="/user/charge/pay/heating/${item.chargeId}"/>">Zapłać</a></td>
        			</c:if>
        			<c:if test="${item.status == true}">
        				<td>Zapłacono</td>
        				<td><a class="btnNotActive" href="<c:url value="/user/charge/pay/heating/${item.chargeId}"/>">Zapłać</a></td>
        			</c:if>
				</tr>
			</c:forEach>
			<c:forEach items="${listChargeOther}" var="item">
				<tr>
					<td>${item.name}</td>
        			<td>${item.paymentDate}</td>
        			<c:if test="${item.status == false}">
        				<td>Nie zapłacono</td>
        				<td><a class="btnActive" href="<c:url value="/user/charge/pay/other/${item.chargeId}"/>">Zapłać</a></td>
        			</c:if>
        			<c:if test="${item.status == true}">
        				<td>Zapłacono</td>
        				<td><a class="btnNotActive" href="<c:url value="/user/charge/pay/other/${item.chargeId}"/>">Zapłać</a></td>
        			</c:if>
				</tr>
			</c:forEach>
			<c:forEach items="${listChargeSevage}" var="item">
				<tr>
					<td>${item.name}</td>
        			<td>${item.paymentDate}</td>
        			<c:if test="${item.status == false}">
        				<td>Nie zapłacono</td>
        				<td><a class="btnActive" href="<c:url value="/user/charge/pay/sevage/${item.chargeId}"/>">Zapłać</a></td>
        			</c:if>
        			<c:if test="${item.status == true}">
        				<td>Zapłacono</td>
        				<td><a class="btnNotActive" href="<c:url value="/user/charge/pay/sevage/${item.chargeId}"/>">Zapłać</a></td>
        			</c:if>
				</tr>
			</c:forEach>
			<c:forEach items="${listChargeWater}" var="item">
				<tr>
					<td>${item.name}</td>
        			<td>${item.paymentDate}</td>
        			<c:if test="${item.status == false}">
        				<td>Nie zapłacono</td>
        				<td><a class="btnActive" href="<c:url value="/user/charge/pay/water/${item.chargeId}"/>">Zapłać</a></td>
        			</c:if>
        			<c:if test="${item.status == true}">
        				<td>Zapłacono</td>
        				<td><a class="btnNotActive" href="<c:url value="/user/charge/pay/water/${item.chargeId}"/>">Zapłać</a></td>
        			</c:if>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<a class="likeabutton" href="<c:url value="/user/charge/report.html?userId=${userId}"/>" target="_blank">Pobierz zestawienie opłat za ostatni miesiąc</a>
	<a style="margin-top: 15px;" class="likeabutton" href="<c:url value="/user/charge/reportYearly.html?userId=${userId}"/>" target="_blank">Pobierz zestawienie opłat za ostatni rok</a>
</div>