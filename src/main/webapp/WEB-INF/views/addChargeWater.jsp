<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="botDetect" uri="botDetect"%>

<div class="col-md-4 col-md-offset-4">
	
	<c:choose>
		<c:when test="${user}">
			<h2>Rachunek za wodę</h2>
		</c:when>
		<c:otherwise>
			<h2>Naliczanie opłaty za wodę</h2>
		</c:otherwise>
	</c:choose>
			
	<c:if test="${user}">
		<script>
			window.onload = function(){
				document.getElementById("name").readOnly = true;
				document.getElementById("paymentDate").readOnly = true;
				document.getElementById("initialStateCold").readOnly = true;
				document.getElementById("initialStateHot").readOnly = true;
				document.getElementById("amountToPay").readOnly = true;
				document.getElementById("pricePerUnitCold").readOnly = true;
				document.getElementById("pricePerUnitHot").readOnly = true;
				document.getElementById("lumpSumCold").readOnly = true;
				document.getElementById("lumpSumHot").readOnly = true;
				document.getElementById("priceDiv").style.display = 'none';
				document.getElementById("priceDiv2").style.display = 'none';
			};
		</script>
	</c:if>
		
	<form:form method="POST" modelAttribute="chargeWater">
		<div class="form-group">
	    	<label for="name">Nazwa opłaty:</label>
	    	<c:choose>
					<c:when test="${user}">
						<form:input path="name" id="name" cssClass="form-control"/>
					</c:when>
 				<c:otherwise>
 					<form:input path="name" id="name" value="Opłata za wodę | [miesiąc]" cssClass="form-control"/>
 				</c:otherwise>
				</c:choose>
			<form:errors path="name" cssClass="error"/>
		</div>
		<div class="form-group">
	    	<label for="paymentDate">Data płatności:</label>
	    	<form:input path="paymentDate" id="paymentDate" cssClass="form-control"/>
			<form:errors path="paymentDate" cssClass="error"/>
	  	</div>
	  	<div class="form-group">
	    	<label for="lumpSumCold">Ryczałt za zimną wodę:</label>
	    	<form:input path="lumpSumCold" id="lumpSumCold" cssClass="form-control"/>
			<form:errors path="lumpSumCold" cssClass="error"/>
	  	</div>
	  	<div class="form-group">
	    	<label for="lumpSumHot">Ryczałt za ciepłą wodę:</label>
	    	<form:input path="lumpSumHot" id="lumpSumHot" cssClass="form-control"/>
			<form:errors path="lumpSumHot" cssClass="error"/>
	  	</div>
	  	<c:choose>
			<c:when test="${user}">
				<div class="form-group">
		   			<label for="initialStateCold">Poprzedni stan licznika(woda zimna):</label>
		   			<form:input path="initialStateCold" id="initialStateCold" cssClass="form-control"/>
					<form:errors path="initialStateCold" cssClass="error"/>
				</div>
				<div class="form-group">
		   			<label for="initialStateHot">Poprzedni stan licznika(woda ciepła):</label>
		   			<form:input path="initialStateHot" id="initialStateHot" cssClass="form-control"/>
					<form:errors path="initialStateHot" cssClass="error"/>
				</div>
				<c:choose>
	  				<c:when test="${lumpSum}">
	  					<div class="form-group">
			    			<label for="finalStateCold">Obecny stan licznika(woda zimna):</label>
			    			<form:input path="finalStateCold" id="finalStateCold" cssClass="form-control"/>
							<form:errors path="finalStateCold" cssClass="error"/>
			  			</div>
	  				</c:when>
	  				<c:otherwise>
	  					<div class="form-group">
			    			<label for="finalStateCold">Obecny stan licznika(woda zimna):</label>
			    			<form:input onkeyup="findTotal()" path="finalStateCold" id="finalStateCold" cssClass="form-control"/>
							<form:errors path="finalStateCold" cssClass="error"/>
			  			</div>
	  				</c:otherwise>
	  			</c:choose>
				<c:choose>
	  				<c:when test="${lumpSum}">
	  					<div class="form-group">
			    			<label for="finalStateHot">Obecny stan licznika(woda ciepła):</label>
			    			<form:input path="finalStateHot" id="finalStateHot" cssClass="form-control"/>
							<form:errors path="finalStateHot" cssClass="error"/>
			  			</div>
	  				</c:when>
	  				<c:otherwise>
	  					<div class="form-group">
			    			<label for="finalStateHot">Obecny stan licznika(woda ciepła):</label>
			    			<form:input onkeyup="findTotal()" path="finalStateHot" id="finalStateHot" cssClass="form-control"/>
							<form:errors path="finalStateHot" cssClass="error"/>
			  			</div>
	  				</c:otherwise>
	  			</c:choose>
				<div class="form-group" id="priceDiv">
		   			<label for="pricePerUnitCold">Cena za jednostkę zimnej wody:</label>
		   			<form:input path="pricePerUnitCold" id="pricePerUnitCold" cssClass="form-control"/>
					<form:errors path="pricePerUnitCold" cssClass="error"/>
				</div>
				<div class="form-group" id="priceDiv2">
		   			<label for="pricePerUnitHot">Cena za jednostkę ciepłej wody:</label>
		   			<form:input path="pricePerUnitHot" id="pricePerUnitHot" cssClass="form-control"/>
					<form:errors path="pricePerUnitHot" cssClass="error"/>
				</div>
				<div class="form-group">
	    			<label for="amountToPay">Kwota do zapłaty:</label>
	    			<form:input path="amountToPay" id="amountToPay" cssClass="form-control"/>
					<form:errors path="amountToPay" cssClass="error"/>
	  			</div>
	  			<div class="form-group">
	  				<botDetect:captcha id="exampleCaptcha"/>
	  				<input class="form-control" id="captchaCode" type="text" name="captchaCode" value="${basicExample.captchaCode}"/>
	  				<span class="error">${error}</span>
					<form:errors path="captchaCode" cssClass="error"/>
	  			</div>
 			</c:when>
 			<c:otherwise>
 				<div class="form-group">
		   			<label for="pricePerUnitCold">Cena za jednostkę zimnej wody:</label>
		   			<form:input path="pricePerUnitCold" id="pricePerUnitCold" cssClass="form-control"/>
					<form:errors path="pricePerUnitCold" cssClass="error"/>
				</div>
				<div class="form-group">
		   			<label for="pricePerUnitHot">Cena za jednostkę ciepłej wody:</label>
		   			<form:input path="pricePerUnitHot" id="pricePerUnitHot" cssClass="form-control"/>
					<form:errors path="pricePerUnitHot" cssClass="error"/>
				</div>
 			</c:otherwise>
 		</c:choose>
 		
 		<script type="text/javascript">
			function findTotal(){
			    var initialStateCold = document.getElementById('initialStateCold').value;
			    var initialStateHot = document.getElementById('initialStateHot').value;
			    var finalStateCold = document.getElementById('finalStateCold').value;
			    var finalStateHot = document.getElementById('finalStateHot').value;
			    var pricePerUnitCold = document.getElementById('pricePerUnitCold').value;
			    var pricePerUnitHot = document.getElementById('pricePerUnitHot').value;
			    var amountToPay = 0.0;
			    
			    amountToPay = parseFloat(pricePerUnitCold * (finalStateCold - initialStateCold) 
			    		+ pricePerUnitHot * (finalStateHot - initialStateHot)).toFixed(2);
  
			    document.getElementById('amountToPay').value = amountToPay;
			}
	    </script>
	    <script type="text/javascript">
		    function isEmpty() {
				if(document.getElementById('finalStateCold').value == "") {
					document.getElementById('finalStateCold').value = "0.0"
				}
				if(document.getElementById('finalStateHot').value == "") {
					document.getElementById('finalStateHot').value = "0.0"
				}
			}
	    </script>
		<c:if test="${lumpSum}">
			<script type="text/javascript">
				document.getElementById("finalStateCold").readOnly = true;
				document.getElementById("finalStateHot").readOnly = true;
			    var initialStateCold = document.getElementById('initialStateCold').value;
			    var initialStateHot = document.getElementById('initialStateHot').value;
			    var lumpSumCold = document.getElementById('lumpSumCold').value;
			    var lumpSumHot = document.getElementById('lumpSumHot').value;
			    var finalStateCold = parseFloat(initialStateCold) + parseFloat(lumpSumCold);
			    var finalStateHot = parseFloat(initialStateHot) + parseFloat(lumpSumHot);
			    var pricePerUnitCold = document.getElementById('pricePerUnitCold').value;
			    var pricePerUnitHot = document.getElementById('pricePerUnitHot').value;
			    var amountToPay = 0.0;
			    
			    document.getElementById('finalStateCold').value = finalStateCold;
			    document.getElementById('finalStateHot').value = finalStateHot;
			    
			    amountToPay = parseFloat(pricePerUnitCold * (finalStateCold - initialStateCold) 
			    		+ pricePerUnitHot * (finalStateHot - initialStateHot)).toFixed(2);
			    
			    document.getElementById('amountToPay').value = amountToPay;
			</script>
		</c:if>  
		<c:choose>
			<c:when test="${user}">
				<button type="submit" onclick="isEmpty()" class="btn btn-default" style="width: 100%;">Opłać rachunek</button>
			</c:when>
			<c:otherwise>
				<button type="submit" onclick="isEmpty()" class="btn btn-default" style="width: 100%;">Nalicz opłatę</button>
			</c:otherwise>
		</c:choose>
 			
		<script>
			$(function() {
		    	$( "#paymentDate" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();		    
		  	});
		</script>	
	</form:form>
</div>