<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="row">
	<h2>Lista budynków</h2>
	<c:choose>
 		<c:when test="${flag}">
			<div class="alert alert-success" role="alert">
		    	<strong>${success}</strong>
			</div>
		</c:when>
	</c:choose>
	<table class="table table-bordered">
    	<thead>
      		<tr>
        		<th>Id</th>
        		<th>Miasto</th>
        		<th>Ulica</th>
        		<th>Numer budynku</th>
        		<th>Mieszkania</th>
        		<th>Edytuj</th>
        		<th>Usuń</th>
			</tr>
		</thead>
    	<tbody>
    		<c:forEach items="${buildings}" var="building">
      		<tr>
        		<td>${building.buildingId}</td>
        		<td>${building.city}</td>
        		<td>${building.street}</td>
        		<td>${building.number}</td>
        		<td><a href="adres">Wyświetl</a></td>
        		<td>
        			<a href="<c:url value="/admin/building/edit/${building.buildingId}"/>">
        				<img src="<c:url value="/resources/images/edytuj.png"/>"/>Edytuj
        			</a>
        		</td>
        		<td>
        			<a href="<c:url value="/admin/building/delete/${building.buildingId}"/>">
        				<img src="<c:url value="/resources/images/usun.png"/>"/>Usuń
        			</a>
        		</td>
      		</tr>
      		</c:forEach>
      	</tbody>
	</table>
</div>