<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<script>
$(document).ready(function(){
  $('#filterTable').dataTable().yadcf([
		{column_number : 0},
	    {column_number : 2},
		{column_number : 5}]);
});
</script>

<div class="row">
	<h2>Naliczone opłaty</h2>
	<table class="table table-bordered" id="filterTable">
    	<thead>
      		<tr>
      			<th>Budynek</th>
      			<th>Numer mieszkania</th>
      			<th>Typ opłaty</th>
        		<th id="condition_filter">Nazwa opłaty</th>
        		<th id="price_filter">Termin płatności</th>
        		<th>Status</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${listChargeElectricity}" var="item"  varStatus="status">
				<tr>
					<td>${listUsersWithChargesElectricity[status.index].street} ${listUsersWithChargesElectricity[status.index].buildingNumber},${listUsersWithChargesElectricity[status.index].city}</td>
					<td>${listUsersWithChargesElectricity[status.index].flatNumber}</td>
					<td>Prąd</td>
					<td>${item.name}</td>
        			<td>${item.paymentDate}</td>
        			<c:choose>
	        			<c:when test="${item.status == true}">
	        				<td>Zapłacono</td>
	        			</c:when>
	        			<c:otherwise>
	        				<td>Nie zapłacono</td>
	        			</c:otherwise>
        			</c:choose>
				</tr>
			</c:forEach>
			<c:forEach items="${listChargeGarbage}" var="item" varStatus="status">
				<tr>
					<td>${listUsersWithChargesGarbage[status.index].street} ${listUsersWithChargesGarbage[status.index].buildingNumber},${listUsersWithChargesGarbage[status.index].city}</td>
					<td>${listUsersWithChargesGarbage[status.index].flatNumber}</td>
					<td>Odpady</td>
					<td>${item.name}</td>
        			<td>${item.paymentDate}</td>
        			<c:choose>
	        			<c:when test="${item.status == true}">
	        				<td>Zapłacono</td>
	        			</c:when>
	        			<c:otherwise>
	        				<td>Nie zapłacono</td>
	        			</c:otherwise>
        			</c:choose>
				</tr>
			</c:forEach>
			<c:forEach items="${listChargeGas}" var="item" varStatus="status">
				<tr>
					<td>${listUsersWithChargesGas[status.index].street} ${listUsersWithChargesGas[status.index].buildingNumber},${listUsersWithChargesGas[status.index].city}</td>
					<td>${listUsersWithChargesGas[status.index].flatNumber}</td>
					<td>Gaz</td>
					<td>${item.name}</td>
        			<td>${item.paymentDate}</td>
        			<c:choose>
	        			<c:when test="${item.status == true}">
	        				<td>Zapłacono</td>
	        			</c:when>
	        			<c:otherwise>
	        				<td>Nie zapłacono</td>
	        			</c:otherwise>
        			</c:choose>
				</tr>
			</c:forEach>
			<c:forEach items="${listChargeHeating}" var="item" varStatus="status">
				<tr>
					<td>${listUsersWithChargesHeating[status.index].street} ${listUsersWithChargesHeating[status.index].buildingNumber},${listUsersWithChargesHeating[status.index].city}</td>
					<td>${listUsersWithChargesHeating[status.index].flatNumber}</td>
					<td>Ogrzewanie</td>
					<td>${item.name}</td>
        			<td>${item.paymentDate}</td>
        			<c:choose>
	        			<c:when test="${item.status == true}">
	        				<td>Zapłacono</td>
	        			</c:when>
	        			<c:otherwise>
	        				<td>Nie zapłacono</td>
	        			</c:otherwise>
        			</c:choose>
				</tr>
			</c:forEach>
			<c:forEach items="${listChargeOther}" var="item" varStatus="status">
				<tr>
					<td>${listUsersWithChargesOther[status.index].street} ${listUsersWithChargesOther[status.index].buildingNumber},${listUsersWithChargesOther[status.index].city}</td>
					<td>${listUsersWithChargesOther[status.index].flatNumber}</td>
					<td>Inne</td>
					<td>${item.name}</td>
        			<td>${item.paymentDate}</td>
        			<c:choose>
	        			<c:when test="${item.status == true}">
	        				<td>Zapłacono</td>
	        			</c:when>
	        			<c:otherwise>
	        				<td>Nie zapłacono</td>
	        			</c:otherwise>
        			</c:choose>
				</tr>
			</c:forEach>
			<c:forEach items="${listChargeSevage}" var="item" varStatus="status">
				<tr>
					<td>${listUsersWithChargesSevage[status.index].street} ${listUsersWithChargesSevage[status.index].buildingNumber},${listUsersWithChargesSevage[status.index].city}</td>
					<td>${listUsersWithChargesSevage[status.index].flatNumber}</td>
					<td>Ścieki</td>
					<td>${item.name}</td>
        			<td>${item.paymentDate}</td>
        			<c:choose>
	        			<c:when test="${item.status == true}">
	        				<td>Zapłacono</td>
	        			</c:when>
	        			<c:otherwise>
	        				<td>Nie zapłacono</td>
	        			</c:otherwise>
        			</c:choose>
				</tr>
			</c:forEach>
			<c:forEach items="${listChargeWater}" var="item" varStatus="status">
				<tr>
					<td>${listUsersWithChargesWater[status.index].street} ${listUsersWithChargesWater[status.index].buildingNumber},${listUsersWithChargesWater[status.index].city}</td>
					<td>${listUsersWithChargesWater[status.index].flatNumber}</td>
					<td>Woda</td>
					<td>${item.name}</td>
        			<td>${item.paymentDate}</td>
        			<c:choose>
	        			<c:when test="${item.status == true}">
	        				<td>Zapłacono</td>
	        			</c:when>
	        			<c:otherwise>
	        				<td>Nie zapłacono</td>
	        			</c:otherwise>
        			</c:choose>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>
	