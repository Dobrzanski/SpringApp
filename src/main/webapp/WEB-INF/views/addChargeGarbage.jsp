<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="botDetect" uri="botDetect"%>

<div class="col-md-4 col-md-offset-4">
	<c:choose>
		<c:when test="${user}">
			<h2>Rachunek za śmieci</h2>
		</c:when>
		<c:otherwise>
			<h2>Naliczanie opłaty za śmieci</h2>
		</c:otherwise>
	</c:choose>
	<c:if test="${user}">
		<script>
			window.onload = function(){
				document.getElementById("name").readOnly = true;
				document.getElementById("paymentDate").readOnly = true;
				document.getElementById("amountToPay").readOnly = true;
				document.getElementById("pricePerPerson").readOnly = true;
				document.getElementById("priceDiv").style.display = 'none';
				document.getElementById("personDiv").style.display = 'none';
			};
		</script>
	</c:if>		
	<form:form method="POST" modelAttribute="chargeGarbage">
		<div class="form-group">
	    	<label for="name">Nazwa opłaty:</label>
	    	<c:choose>
					<c:when test="${user}">
						<form:input path="name" id="name" cssClass="form-control"/>
					</c:when>
 				<c:otherwise>
 					<form:input path="name" id="name" value="Opłata za śmieci | [miesiąc]" cssClass="form-control"/>
 				</c:otherwise>
				</c:choose>
			<form:errors path="name" cssClass="error"/>
	  	</div>
		<div class="form-group">
	    	<label for="paymentDate">Data płatności:</label>
	    	<form:input path="paymentDate" id="paymentDate" cssClass="form-control"/>
			<form:errors path="paymentDate" cssClass="error"/>
	  	</div>
	  	<c:choose>
			<c:when test="${user}">
				<div class="form-group" id="priceDiv">
			    	<label for="pricePerPerson">Cena za osobę:</label>
			    	<form:input path="pricePerPerson" id="pricePerPerson" cssClass="form-control"/>
					<form:errors path="pricePerPerson" cssClass="error"/>
			  	</div>
			  	<div class="form-group" id="personDiv">
			    	<label for="numberOfPeople">Liczba osób:</label>
			    	<form:input path="numberOfPeople" id="numberOfPeople" cssClass="form-control"/>
					<form:errors path="numberOfPeople" cssClass="error"/>
			  	</div>
				<div class="form-group">
		   			<label for="amountToPay">Kwota do zapłaty:</label>
		   			<form:input path="amountToPay" id="amountToPay" cssClass="form-control"/>
					<form:errors path="amountToPay" cssClass="error"/>
				</div>
				<div class="form-group">
					<botDetect:captcha id="exampleCaptcha"/>
					<input class="form-control" id="captchaCode" type="text" name="captchaCode" value="${basicExample.captchaCode}"/>
					<span class="error">${error}</span>
					<form:errors path="captchaCode" cssClass="error"/>
				</div>
			</c:when>
			<c:otherwise>
				<div class="form-group">
		   			<label for="pricePerPerson">Cena za osobę:</label>
		   			<form:input path="pricePerPerson" id="pricePerPerson" cssClass="form-control"/>
					<form:errors path="pricePerPerson" cssClass="error"/>
				</div>
			</c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${user}">
				<button type="submit" class="btn btn-default" style="width: 100%;">Opłać rachunek</button>
			</c:when>
			<c:otherwise>
				<button type="submit" class="btn btn-default" style="width: 100%;">Nalicz opłatę</button>
			</c:otherwise>
		</c:choose>
 			
		<script>
			$(function() {
		    	$( "#paymentDate" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();		    
		  	});
		</script>	
	</form:form>
</div>