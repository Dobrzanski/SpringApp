<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="container">
	<div class="row">
		<div id="myCarousel" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
		    <ol class="carousel-indicators">
		    	<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		      	<li data-target="#myCarousel" data-slide-to="1"></li>
		      	<li data-target="#myCarousel" data-slide-to="2"></li>
		    </ol>
		
		    <!-- Wrapper for slides -->
		    <div class="carousel-inner" role="listbox">
		    	<div class="item active">
		        	<img src="<c:url value="/resources/images/slajd1.JPG"/>" alt="slajd1" width="1175">
		      	</div>
		
		      	<div class="item">
		        	<img src="<c:url value="/resources/images/slajd2.JPG"/>" alt="slajd2" width="1175">
		      	</div>
		    
		      	<div class="item">
		        	<img src="<c:url value="/resources/images/slajd3.JPG"/>" alt="slajd3" width="1175">
		      	</div>
			</div>
		
		    <!-- Left and right controls -->
		    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
		      	<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		      	<span class="sr-only">Previous</span>
		    </a>
		    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
		      	<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		      	<span class="sr-only">Next</span>
		    </a>
		</div>
	</div>
</div>
