<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="header">
	<span style="float: right; margin-top: 14px;">
    	<a href="?lang=pl"><img alt="PL" src="<c:url value="/resources/images/pl_PL.png"/>"></a> | 
    	<a href="?lang=en"><img alt="EN" src="<c:url value="/resources/images/en_GB.png"/>"></a> | 
    	<a href="?lang=de"><img alt="DE" src="<c:url value="/resources/images/de_DE.png"/>"></a>
	</span>
</div>