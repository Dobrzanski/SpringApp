<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="row">
	<h2>Lista mieszkań</h2>
	<c:choose>
 		<c:when test="${flag}">
			<div class="alert alert-success" role="alert">
		    	<strong>${success}</strong>
			</div>
		</c:when>
	</c:choose>
	<table class="table table-bordered">
    	<thead>
      		<tr>
        		<th>Id</th>
        		<th>Miasto</th>
        		<th>Ulica</th>
        		<th>Numer budynku</th>
        		<th>Numer mieszkania</th>
        		<th>Liczba lokatorów</th>
        		<th>Adres e-mail</th>
        		<th>Szczegóły</th>
        		<th>Edytuj</th>
        		<th>Usuń</th>
			</tr>
		</thead>
    	<tbody>
    		<c:forEach items="${users}" var="user">
      		<tr>
        		<td>${user.userId}</td>
        		<td>${user.city}</td>
        		<td>${user.street}</td>
        		<td>${user.buildingNumber}</td>
        		<td>${user.flatNumber}</td>
        		<td>${user.peopleNumber}</td>
        		<td>${user.mail}</td>
        		<td><a href="adres">Szczegóły</a></td>
        		<td>
        			<a href="<c:url value="/admin/user/edit/${user.userId}"/>">
        				<img src="<c:url value="/resources/images/edytuj.png"/>"/>Edytuj
        			</a>
        		</td>
        		<td>
        			<a href="<c:url value="/admin/user/delete/${user.userId}"/>">
        				<img src="<c:url value="/resources/images/usun.png"/>"/>Usuń
        			</a>
        		</td>
      		</tr>
      		</c:forEach>
      	</tbody>
	</table>
</div>