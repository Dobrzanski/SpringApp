<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div class="col-md-10 col-md-offset-1" >
	<c:choose>
 		<c:when test="${edit}">
  			<h2>Aktualizacja danych użytkownika</h2>
  		</c:when>
  		<c:otherwise>
			<h2>Dodawanie użytkownika</h2>
		</c:otherwise>
	</c:choose>
	<c:choose>
 		<c:when test="${flag}">
			<div class="alert alert-danger" role="alert">
		    	<strong>Istnieje już użytkownik przypisany do podanego adresu e-mail!</strong>
			</div>
		</c:when>
	</c:choose>	
	<form:form method="POST" modelAttribute="user">
		<div class="col-md-3 col-md-offset-1">
		<c:choose>
 			<c:when test="${disabled}">
				<script>
					window.onload = function(){
						document.getElementById("firstname").readOnly = true;
						document.getElementById("surname").readOnly = true;
						document.getElementById("mail").readOnly = true;
						document.getElementById("city").readOnly = true;
						document.getElementById("street").readOnly = true;
						document.getElementById("buildingNumber").readOnly = true;
						document.getElementById("flatNumber").readOnly = true;
						document.getElementById("stateColdWater").readOnly = true;
						document.getElementById("stateHotWater").readOnly = true;
						document.getElementById("stateHeating").readOnly = true;
						document.getElementById("stateSevage").readOnly = true;
						document.getElementById("stateElectricity").readOnly = true;
						document.getElementById("stateGas").readOnly = true;
					};
				</script>
			</c:when>
		</c:choose>
			<h3>Mieszkanie</h3>
			<div class="form-group">
				<label for="street">Miasto:</label>
				<form:select path="city" id="city" cssClass="form-control">
					<form:option value="NONE" label="--- Wybierz ---" disabled="true" selected="selected"/>
				   	<form:options items="${uniqueCities}" />
				</form:select>
				<form:errors path="city" cssClass="error"/>
			</div>
			<div class="form-group">
				<label for="street">Nazwa ulicy:</label>
				<form:select path="street" id="street" cssClass="form-control">
					<form:option value="NONE" label="--- Wybierz ---" disabled="true" selected="selected"/>
				   	<form:options items="${uniqueStreets}" />
				</form:select>
				<form:errors path="street" cssClass="error"/>
			</div>
			<div class="form-group">
				<label for="buildingNumber">Numer budynku:</label>
				<form:select path="buildingNumber" id="buildingNumber" cssClass="form-control">
					<form:option value="-1" label="--- Wybierz ---" disabled="true" selected="selected"/>
				   	<form:options items="${uniqueNumbers}" />
				</form:select>
				<form:errors path="buildingNumber" cssClass="error"/>
			</div>
			<div class="form-group">
				<label for="flatNumber">Numer mieszkania:</label>
				<form:select path="flatNumber" id="flatNumber" cssClass="form-control">
					<form:option value="-1" label="--- Wybierz ---" disabled="true" selected="selected"/>
				   	<form:option value="1">1</form:option>
				   	<form:option value="2">2</form:option>
				   	<form:option value="3">3</form:option>
				   	<form:option value="4">4</form:option>
				   	<form:option value="5">5</form:option>
				   	<form:option value="6">6</form:option>
				   	<form:option value="7">7</form:option>
				   	<form:option value="8">8</form:option>
				   	<form:option value="9">9</form:option>
				   	<form:option value="10">10</form:option>
				   	<form:option value="11">11</form:option>
				   	<form:option value="12">12</form:option>
				   	<form:option value="13">13</form:option>
				   	<form:option value="14">14</form:option>
				   	<form:option value="15">15</form:option>
				</form:select>
				<form:errors path="flatNumber" cssClass="error"/>
			</div>
			<div class="form-group">
				<label for="peopleNumber">Liczba lokatorów:</label>
				<form:select disabled="disabled" path="peopleNumber" cssClass="form-control">
					<form:option value="-1" label="--- Wybierz ---" disabled="true" selected="selected"/>
				   	<form:option value="1">1</form:option>
				   	<form:option value="2">2</form:option>
				   	<form:option value="3">3</form:option>
				   	<form:option value="4">4</form:option>
				   	<form:option value="5">5</form:option>
				   	<form:option value="6">6</form:option>
				   	<form:option value="7">7</form:option>
				   	<form:option value="8">8</form:option>
				   	<form:option value="9">9</form:option>
				   	<form:option value="10">10</form:option>
				</form:select>
				<form:errors path="peopleNumber" cssClass="error"/>
			</div>
		</div>
		<div class="col-md-3 col-md-offset-1">
			<h3>Kontakt</h3>
			<div class="form-group">
				<label for="firstname">Imię:</label>
				<form:input path="firstname" id="firstname" cssClass="form-control"/>
				<form:errors path="firstname" cssClass="error"/>
			</div>
			<div class="form-group">
				<label for="surname">Nazwisko:</label>
		    	<form:input path="surname" id="surname" cssClass="form-control"/>
				<form:errors path="surname" cssClass="error"/>
			</div>
			<div class="form-group">
				<label for="mail">Adres e-mail:</label>
		    	<form:input path="mail" id="mail" cssClass="form-control"/>
				<form:errors path="mail" cssClass="error"/>
			</div>
			<c:choose>
 				<c:when test="${edit}">
  					<div class="form-group">	
						<label for="password">Hasło:</label>
					    <form:input type="password" path="password" id="password" cssClass="form-control" value=""/>
						<form:errors path="password" cssClass="error"/>
					</div>
  				</c:when>
  				<c:otherwise>
					<div class="form-group">	
						<label for="password">Hasło:</label>
					    <form:input type="password" path="password" id="password" cssClass="form-control" value="${randomPassword}"/>
						<form:errors path="password" cssClass="error"/>
					</div>
				</c:otherwise>
			</c:choose>
			<div class="form-group">
				<label for="phone">Numer telefonu:</label>
		    	<form:input path="phone" id="phone" cssClass="form-control"/>
				<form:errors path="phone" cssClass="error"/>
			</div>
			<div style="display:none;">
				<form:label path="enabled">AKTYWNY?</form:label>
	        	<form:checkbox path="enabled" checked="checked"/>
	        	<form:errors path="enabled"/>
        	</div>
		</div>
		<div class="col-md-3 col-md-offset-1">
			<h3>Liczniki</h3>
			<div class="form-group">
				<label for="stateColdWater">Zimna woda:</label>
		    	<form:input path="stateColdWater" id="stateColdWater" cssClass="form-control"/>
				<form:errors path="stateColdWater" cssClass="error"/>
			</div>
			<div class="form-group">
				<label for="stateHotWater">Ciepła woda:</label>
		    	<form:input path="stateHotWater" id="stateHotWater" cssClass="form-control"/>
				<form:errors path="stateHotWater" cssClass="error"/>
			</div>
			<div class="form-group">
				<label for="stateElectricity">Prąd:</label>
		    	<form:input path="stateElectricity" id="stateElectricity" cssClass="form-control"/>
				<form:errors path="stateElectricity" cssClass="error"/>
			</div>
  			<div class="form-group">	
				<label for="stateGas">Gaz:</label>
				<form:input type="text" path="stateGas" id="stateGas" cssClass="form-control" value=""/>
				<form:errors path="stateGas" cssClass="error"/>
			</div>		
			<div class="form-group">
				<label for="stateHeating">Ogrzewanie:</label>
		    	<form:input path="stateHeating" id="stateHeating" cssClass="form-control"/>
				<form:errors path="stateHeating" cssClass="error"/>
			</div>
			<div class="form-group">
				<label for="stateSevage">Ścieki:</label>
		    	<form:input path="stateSevage" id="stateSevage" cssClass="form-control" value=""/>
				<form:errors path="stateSevage" cssClass="error"/>
			</div>
		</div>
		<div class="col-md-11 col-md-offset-1">
		<c:choose>
			<c:when test="${edit}">
				<button type="submit" class="btn btn-default" style="width: 100%;">Aktualizuj dane</button>
  			</c:when>
  			<c:otherwise>
				<button type="submit" class="btn btn-default" style="width: 100%;">Dodaj użytkownika</button>
			</c:otherwise>
		</c:choose>
		</div>
	</form:form>
</div>