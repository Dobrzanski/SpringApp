<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
<link href="<c:url value="/resources/font-awesome-4.3.0/css/font-awesome.min.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/custom.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/jquery-ui.css" />" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script src="<c:url value="/resources/js/custom.js" />"></script>
<script src="<c:url value="/resources/js/jquery-1.10.2.js" />"></script>
<script src="<c:url value="/resources/js/jquery-ui.js" />"></script>

<script src="<c:url value="/resources/column_filter/jquery.dataTables.min.js" />"></script>
<script src="<c:url value="/resources/column_filter/jquery.dataTables.yadcf.js" />"></script>
<link href="<c:url value="/resources/column_filter/jquery.datatables.yadcf.css" />" rel="stylesheet">

<script>
$(function() {
	$(".navbar li").on("click", function() {
		$(".navbar li").removeClass("active");
	    $(this).addClass("active");
	});
});
</script>
<title><tiles:insertAttribute name="title" ignore="true" /></title>
</head>
<body>
<div class="container">
	<div class="row">
		<tiles:insertAttribute name="header" />
	</div>
</div>
<nav class="navbar navbar-inverse navbar-fixed-top">
	<tiles:insertAttribute name="menu" />
</nav>
<div class="container" style="margin-bottom: 60px;">
<div><tiles:insertAttribute name="menu_left" /></div>
	<tiles:insertAttribute name="body" />
</div>
<footer class="footer">
	<tiles:insertAttribute name="footer" />
</footer> 
</body>
<script>
    var url = window.location;
    // Will only work if string in href matches with location
    $('.sidebar-nav a[href="'+ url +'"]').parent().addClass('active');

    // Will also work for relative and absolute hrefs
    $('.sidebar-nav a').filter(function() {
        return this.href == url;
    }).parent().addClass('active');
</script>
</html>