<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<div class="container">
	<div class="navbar-header">
    	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	    	<span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<c:url value="/"/>"><img style="height: 50px;" alt="SM" src="<c:url value="/resources/images/logo.png"/>"></a>
	</div>
    <div id="navbar" class="collapse navbar-collapse">
    	<div class="navbar-left">
	    	<ul class="nav navbar-nav">
		    	<li class="active"><a href="<c:url value="/"/>"><spring:message code="nav.homepage"/></a></li>
		       	<li><a href="<c:url value="/about"/>"><spring:message code="nav.about"/></a></li>
	        	<li><a href="<c:url value="/contact"/>"><spring:message code="nav.contact"/></a></li>
			</ul>
		</div>
		<c:url value="/j_spring_security_logout" var="logoutUrl" />
 
		<!-- csrt for log out-->
		<form action="${logoutUrl}" method="post" id="logoutForm">
		  <input type="hidden" 
			name="${_csrf.parameterName}"
			value="${_csrf.token}" />
		</form>
	 
		<script>
			function formSubmit() {
				document.getElementById("logoutForm").submit();
			}
		</script>
		
		<div class="navbar-right">
			<ul class="nav navbar-nav">
				<c:if test="${pageContext.request.userPrincipal.name == null}">
		    	<li><a href="<c:url value="/login"/>"><spring:message code="nav.loginBtn"/></a></li>
		    	</c:if>
		    	<c:if test="${pageContext.request.userPrincipal.name != null}">
		    		<li><a href="<c:url value="/logout"/>"><spring:message code="nav.logout"/></a></li>
		    	</c:if>
			</ul>
			<!--
            <div class="btn-group">
                <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                    <spring:message code="nav.loginBtn"/><span class="caret"></span>
                </button>
                <div class="dropdown-menu" >
                <form name='loginForm' action="<c:url value='j_spring_security_check' />" method='POST'>
                    <div class="col-sm-12" style="padding-top: 15px;">
                        <div class="col-sm-12">
                            <input type="text" placeholder="<spring:message code="nav.mail"/>" onclick="return false;" class="form-control input-sm" name='username' id="inputError" />
                        </div>
                        <br/>
                        <div class="col-sm-12">
                            <input type="password" placeholder="<spring:message code="nav.loginPwd"/>" class="form-control input-sm" name="password" id="Password1" />
                        </div>
                        <div class="col-sm-12">
                            <button name="submit" type="submit" value="submit" class="btn btn-success btn-sm"><spring:message code="nav.loginBtn"/></button>
                        </div>
                    </div>
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                </form>
                </div>
            </div>
            -->
        </div>
	</div><!--/.nav-collapse -->
</div>