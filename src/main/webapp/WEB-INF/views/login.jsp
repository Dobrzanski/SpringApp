<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<title>Login Page</title>

</head>
<body onload='document.loginForm.mail.focus();'>
 
	<h2>Logowanie</h2>
	<div class="col-md-4 col-md-offset-4">
		<c:if test="${not empty error}">
			<div class="alert alert-danger" role="alert"><strong>${error}</strong></div>
		</c:if>
		<c:if test="${not empty msg}">
			<div class="alert alert-success" role="alert"><strong>${msg}</strong></div>
		</c:if>
 
		<form name='loginForm' action="<c:url value='j_spring_security_check' />" method='POST'>
			<div class="form-group">		  
				<label for="mail"><spring:message code="nav.mail"/></label>
				<input class="form-control" type='text' id='mail' name='mail' placeholder="<spring:message code="nav.mail"/>">
			</div>
			<div class="form-group">
				<label for="password"><spring:message code="nav.loginPwd"/></label>
				<input class="form-control" type='password' name='password' placeholder="<spring:message code="nav.loginPwd"/>"/>
			</div>
			<button class="btn btn-default" style="width: 100%;" name="submit" type="submit"><spring:message code="nav.loginBtn"/></button>
		  	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		</form>
	</div>
</body>
</html>