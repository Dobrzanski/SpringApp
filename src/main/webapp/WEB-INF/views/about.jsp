<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="col-md-8 col-md-offset-2">
	<h2>O spółdzielni</h2>
	<p>Nasza Spółdzielnia Mieszkaniowa bierze swój początek w roku 1957, kiedy to wdrożona została w życie odgórna dyrektywa z marca 1957 r. o pomocy państwa dla budownictwa mieszkaniowego ze środków własnych ludności. W ramach powyższego procesu, w Pabianicach powstały trzy ogniska przyszłej spółdzielczości mieszkaniowej – jedno ogólnomiejskie i dwa zakładowe – Pabianickich Zakładów Przemysłu Bawełnianego i Zakładów Tkanin Technicznych.

2 stycznia 1958 r. zwołano zgromadzenie członków założycieli spółdzielni (było ich 49). Miejscem zgromadzenia była sala gimnastyczna Zasadniczej Szkoły Zawodowej przy ul. Gdańskiej (dawnej ul. W. Wasilewskiej) a zarejestrowana spółdzielnia nosiła nazwę Spółdzielnia Mieszkaniowa „Wspólny Dom”.

12 lutego 1958 r. zwołano zgromadzenie członków założycieli (było ich 59) Robotniczej Spółdzielni Mieszkaniowej przy Pabianickich Zakładach Przemysłu Bawełnianego im. Bojowników Rewolucji 1905 r.

15 kwietnia 1958 r. zwołano zgromadzenie członków założycieli (było ich 34) Międzyzakładowej Spółdzielni Mieszkaniowej przy Pabianickich Zakładach Tkanin Technicznych, której w roku 1960 nadano nazwę „Nasza Strzecha”.

W pierwszej połowie 1958 r. wszystkie trzy spółdzielnie uzyskały wpisy w rejestry sądowe.

W sierpniu 1958 r. Spółdzielnia Mieszkaniowa „Wspólny Dom” stała się właścicielką pierwszego budynku mieszkalnego przy ul. Zamkowej 50 (dawniej ul. Armii Czerwonej), którego odbiór techniczny nastąpił w dniu 10 stycznia 1959 r.

W kwietniu 1959 r. do budynku położonego przy ul. św. Rocha (dawniej ul. Gwardii Ludowej 3-5) wprowadzili się pierwsi mieszkańcy Robotniczej Spółdzielni Mieszkaniowej przy Pabianickich Zakładach Przemysłu Bawełnianego im. Bojowników Rewolucji 1905 r.

W marcu 1960 r. przeznaczono do zasiedlenia pierwszy blok mieszkalny Spółdzielni „Nasza Strzecha”, który znajduje się przy ul. kard. S. Wyszyńskiego (dawniej ul. Nowoskromna 6, później ul. XX-Lecia PRL).

22 marca 1962 r. nastąpiło połączenie sił i środków dwóch spółdzielni: „Wspólnego Domu” i „Naszej Strzechy”.

Od dnia 1 kwietnia 1968 r. Robotnicza Spółdzielnia Mieszkaniowa przy Pabianickich Zakładach Przemysłu Bawełnianego im. Bojowników Rewolucji 1905 r. funkcjonowała pod nazwą Robotnicza Spółdzielnia Mieszkaniowa „Włókno”.

W dniu 25 czerwca 1971 r. na połączonym zebraniu przedstawicieli „Wspólnego Domu” i Robotniczej Spółdzielni Mieszkaniowej „Włókno” podjęto uchwałę o połączeniu się obu spółdzielni. W ten sposób powstał jeden spółdzielczy organizm o nazwie Pabianicka Spółdzielnia Mieszkaniowa.

Połączone spółdzielnie mieszkaniowe połączyły się i rozpoczęły duża, wspólną inwestycję – budowę osiedla mieszkaniowego im. Mikołaja Kopernika na Bugaju. Uroczystość wmurowania aktu erekcyjnego pod pierwszy budynek osiedla miała miejsce 20 lipca 1973 r.

Obecnie Pabianicka Spółdzielnia Mieszkaniowa należy do jednej z największych spółdzielni mieszkaniowych w Polsce.
Historia Pabianickiej Spółdzielni Mieszkaniowej opisana jest w książce „Pabianicka Spółdzielnia Mieszkaniowa 1958-2008. Pół wieku w spółdzielczych blokach” autorstwa Kazimierza Brzezińskiego i Andrzeja Gramsza.</p>
</div>