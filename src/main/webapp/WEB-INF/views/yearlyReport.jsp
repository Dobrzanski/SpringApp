<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div class="col-md-10 col-md-offset-1" >
	<h2>Zestawienia roczne</h2>
	<div class="col-md-6 col-md-offset-3">
		<div class="form-group">
			<label for="year">Raport za rok:</label>
			<select id="year" class="form-control">
				<option value="NONE" disabled="true" selected="selected">--- Wybierz ---</option>
			   	<c:forEach var="year" items="${years}">
			   		<option value="${year}">${year}</option>
			   	</c:forEach>
			</select>
		</div>
		<a class="likeabutton">Generuj zestawienie roczne</a>
	</div>
</div>

<script>
	$(document).ready(function(){
	    $("select.form-control").change(function(){
	        var selectedid  = $(".form-control option:selected").val();
	        $('a').attr('href','/jee/admin/charge/yearlyReport/'+selectedid);
	    });
	});
</script>